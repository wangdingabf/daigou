# daigou

一、项目介绍

本项目主要针对个人海外代购以及订货开发，随着海外代购订单日益增多，微信等手工方式已经难以管理，于是，急需一个极其简单，易用的在线的商城进行商品展示，在线下单，在线订单管理系统进行实时订单管理，于是，一个强大的手机端商城及管理系统项目诞生了！

 **

### 完全个人商城，无需任何手续和执照，即可拥有自己的商城和支付宝微信快捷支付功能！！！
** 

本系统最大的优点：

简单！！！

易用！！！

方便！！！

QQ：373717096 

（ **以下地址墙裂建议手机浏览器打开** ）

平台商城地址：http://139.129.222.45/vdinghuo

个人商城地址：http://139.129.222.45/vdinghuo/13331071314

后台管理地址：http://139.129.222.45/vdinghuo/admin/login

 **有兴趣可以自行注册用户进行体验或点击我要体验进行体验** 

 完全开源，近期更新会比较频繁 。大家看到了，star一下，谢谢，本团队会持续更新，一直开源！ 欢迎喜欢的朋友一起来优化功能。

二、软件架构

spring MVC + Mybatis + MySQL + Ehcache+ Jquery + Boostrap等
部署简单，易用！！！

三、业务介绍：

1、主要功能
    店铺配置、支付配置（支付宝微信）、基础配置、商品管理、基础资料、订单管理、订单短信通知，广告管理、支付管理、分析报表。

2、主要流程
    前端商城，
    客户下单支付流程，
    店主确认订单，
    业务员代下单，
    客户管理，
    收发货流程，
    上架发布商品流程等。

四、安装说明：

1，开发环境：
   开发工具：
	Eclipse
	JDK1.8
	Maven
	Mysql5.6以上（linux 注意设置大小写不敏感）
	运行环境：CENTOS6.5以上或windows server 2008、tomcat7以上，JDK1.8， MYSQL5.7

2，按照mvn方式导入

3，数据库还原：步骤 1：还原数据库，2，修改 dbconfig.propities
    3.1 sql导入方式建议 将sql文件粘贴至 查询执行窗口 执行

4，主要技术
    开发语言：JAVA。

5、技术架构
spring MVC + Mybatis + MySQL + Ehcache + Jquery + Boostrap + Ztree等

五、技术支持：

演示环境：http://139.129.222.45/vdinghuo
QQ群：373717096   
 
六、版本管理及更新说明：

此版本为定版1.0
每半月发布一个小版本 例如1.1。
每半年升级大版本 例如2.0

七、开发计划：

    1，后端业务员代下单支持多商品下单

    2，后端订单管理支持多商品订单管理

    3，前端购物车支持多商品下单

    4，商品关联品类，增加品类管理

    5，前后端增加按品类搜索


八、开源及商务合作说明：

1、开源说明：本项目完全遵循GPL V3协议。可以放心从我们的开源版开始使用, 如果你发现产品有任何Bug, 请在Issue模块向我们反馈，谢谢。

2、商务合作：本项目同时支持开源版实施和定制开发服务，商务合作请 统一联系 QQ：373717096

#### 程序截图

平台商城首页：

![平台商城首页](https://images.gitee.com/uploads/images/2018/1206/141516_af59bec6_388403.jpeg "平台首页.jpg")

个人商城首页：

![个人商城首页](https://images.gitee.com/uploads/images/2018/1206/141553_7b5f4e0a_388403.jpeg "商城首页.jpg")

商城商品详情页：

![商城商品详情页](https://images.gitee.com/uploads/images/2018/1206/141829_7d404c68_388403.jpeg "商品详情页.jpg")

商城提交订单页：

![商城提交订单页](https://images.gitee.com/uploads/images/2018/1206/141846_f84ca310_388403.jpeg "提交订单页.jpg")

后台管理端首页：

![首页](https://images.gitee.com/uploads/images/2018/1127/223413_5688c497_388403.jpeg "index.jpg")

订单管理页：

![订单管理](https://images.gitee.com/uploads/images/2018/1127/223344_0e47b805_388403.jpeg "order_list.jpg")

商品管理页：

![商品管理](https://images.gitee.com/uploads/images/2018/1127/223428_4e27249d_388403.jpeg "product_list.jpg")

广告管理页：

![广告管理页](https://images.gitee.com/uploads/images/2018/1206/141922_ba0c6629_388403.jpeg "广告新增.jpg")

个人账户管理页：

![个人账户管理页](https://images.gitee.com/uploads/images/2018/1206/142000_c784ebaa_388403.jpeg "账户管理.jpg")

店铺管理页：

![店铺管理](https://images.gitee.com/uploads/images/2018/1127/223445_06319075_388403.jpeg "shop.jpg")

其他
。。。。。

 **如果您觉得此项目还可以，可以稍微赞助一下，只为演示案例的服务器和短信能够继续支持！！！** 
