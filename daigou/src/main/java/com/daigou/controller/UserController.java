package com.daigou.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.dto.AjaxResult;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Customer;
import com.daigou.model.Product;
import com.daigou.model.ShopCustomer;
import com.daigou.service.CustomerService;
import com.daigou.service.ProductService;
import com.daigou.service.UserService;

/**  
 * @Title: UserController.java
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月16日
 */
@Controller
@RequestMapping("/user")
public class UserController{
	@Autowired
    private UserService userService;
	
	@Autowired
	private ProductService productService;
    /**
     * 跳转我的资料页面
     * @param modelMap
     * @param uId
     * @return
     */
    @RequestMapping(value="/to-user-info/{productId}/{mobile}",method=RequestMethod.GET)
    public String getMyInfoPage(ModelMap modelMap,@PathVariable("productId") Long productId,@PathVariable("mobile") Long mobile){
    	if(mobile != null && mobile != 0L){
    		Customer customer = customerService.selectByMobile(mobile);
    		modelMap.put("userInfo", customer);
    	}
    	Product product = productService.selectByPrimaryKey(productId);
    	if(product != null){
    		modelMap.put("product", product);
    	}
    	modelMap.put("productId", productId);
    	return "/shop/app/user-info";
    }
    @Autowired
	private CustomerService customerService;
    /**
     * 获取客户的资料
     * @param modelMap
     * @param uId
     * @return
     */
    @RequestMapping(value="/user-info/{shopId}/{mobile}",method=RequestMethod.GET)
    @ResponseBody
    public AjaxResult getMyInfo(ModelMap modelMap,@PathVariable("shopId") Long shopId,@PathVariable("mobile") Long mobile){
    	if(mobile != null && mobile != 0L){
    		ShopCustomer customer = customerService.selectByShopIdAndMobile(shopId, mobile);
    		return new AjaxResult(true, customer);
    	}
    	return new AjaxResult(false);
    }
    
    /**
     * 更新客户信息
     * @param modelMap
     * @param uId
     * @return
     */
    @RequestMapping(value="/user-info",method=RequestMethod.POST)
    @ResponseBody
    public AjaxResult updateUserInfo(ModelMap modelMap,@RequestBody ShopCustomer customer){
		ServiceResult result = customerService.insertOrUpdate(customer);
		return new AjaxResult(result);
    }
}
