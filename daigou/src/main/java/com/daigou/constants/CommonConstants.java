package com.daigou.constants;

import com.daigou.tools.PropertiesUtils;


/**
 * 系统常量
 * @author lizhi
 *
 */
public class CommonConstants {  
	
	private CommonConstants() {
		
	}
	public static final String VERIFYCODE = "VERIFY_CODE";
	
	public static final String VERIFYCODETIMES = "VERIFY_CODE_TIMES";
	
	public static final String USER_INFO = "userInfo";
	
	public static final String SHOP_INFO = "shopInfo";
	
	public static final String USER_ID = "userId";
	
	public static final String USER_TOKEN = "userToken";
	
	public static final String LOGIN_NAME = "gou-loginName";
	
	public static final String SHOP_CAR = "gou-shop-car-";
	
	
	/** 获取properties配置文件属性 */
	private static final String CONSTANTSPROPERTIES = "constants.properties";
	// 用static修饰的代码块表示静态代码块，当Java虚拟机（JVM）加载类时，就会执行该代码块
	static {
		PropertiesUtils.readProperties(CONSTANTSPROPERTIES);
	}

	/** 项目路径 */
	private static final String CONTEXTPATH = "contextPath";
	public static final String contextPath = PropertiesUtils.getProperty(CONTEXTPATH);

	/** 静态资源 */
	private static final String STATICSERVER = "staticServer";
	public static final String staticServer = PropertiesUtils.getProperty(STATICSERVER);
	
	/** 标题 */
	private static final String SYSTEM_TITLE = "systemTitle";
	public static final String systemTitle = PropertiesUtils.getProperty(SYSTEM_TITLE);
	
}
