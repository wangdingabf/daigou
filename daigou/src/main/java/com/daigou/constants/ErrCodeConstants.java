package com.daigou.constants;

public class ErrCodeConstants {
	private ErrCodeConstants() {}
	
	public static final int ERRCODE_500 = 500;
	
	public static final int ERRCODE_501 = 501;

	public static final int ERRCODE_502 = 502;
	
	public static final int ERRCODE_503 = 503;
	
	public static final int ERRCODE_504 = 504;
	
	public static final int ERRCODE_505 = 505;
}
