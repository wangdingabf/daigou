package com.daigou.constants;
/**
 * url 常量
 *
 * @author lizhi
 *
 */
public class URLConstants {
	/*
	 * 完善用户信息
	 */
	public static final String DONE_USER_URL = "../admin/done-user";
	
	public static final String DONE_USER_URL_TAG = "/admin/done-user";
	
	public static final String UPLOAD_URL_TAG = "/upload/";
	/*
	 * 完善店铺信息
	 */
	public static final String DONE_SHOP_URL = "../admin/done-shop";
	
	public static final String DONE_SHOP_URL_TAG = "/admin/done-shop";
	/*
	 * 管理平台index
	 */
	public static final String ADMIN_INDEX_URL = "../admin/index";
	
	/*
	 * 管理平台LOGIN
	 */
	public static final String ADMIN_LOGIN_URL = "/admin/login";
	
	/*
	 * 管理平台REGISTER
	 */
	public static final String ADMIN_REGISTER_URL = "../admin/register";
}
