package com.daigou.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.OrderQuery;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.OrderService;
import com.daigou.service.ShopService;
import com.daigou.service.UserService;

/**
 * 
 * 类名称：LoginController   
 * 创建人：李志   
 * 创建时间：2018年02月14日  上午12:16:42  
 * 修改人：李志   
 * 修改时间：2018年04月14日 00:35:17   
 * @version    
 *
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController{
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ShopService shopService;
	
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/index",method = RequestMethod.GET)
    public String toIndexPage(HttpServletRequest request,Model model){
		LOGGER.info("进入后台管理首页");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		model.addAttribute("user", user);
		model.addAttribute("shop", shop);
		
		//报表只统计支付完毕的
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setShopId(shop.getId());
		orderQuery.setTimeType("month");
		orderQuery.builderTime();
		orderQuery.setUser(user);
		//设置分页
		orderQuery.buildPage("tt.");
		Integer totalCount = orderService.selectOrderCount4ReportByCondition(orderQuery);
		model.addAttribute("totalCount", totalCount);
		Order orderSum = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderSum", orderSum==null?new Order():orderSum);
		//统计新建和支付中的也不统计所有的订单,没意义，统计近3个月的
		orderQuery.setTimeType("3");
		orderQuery.builderTime();
		List<Order> orderList = orderService.selectOrder4IndexByCondition(orderQuery);
		model.addAttribute("orderList", orderList);
        return "/admin/app/index";
    }
	
	
	@RequestMapping(value = "/done-user",method = RequestMethod.GET)
    public String toDoneUserPage(HttpServletRequest request,Model model){
		LOGGER.info("进入完善用户信息页面");
		User user = (User)request.getAttribute("userInfo");
		model.addAttribute("user", user);
		return "/admin/app/done_user";
    }
	
	@RequestMapping(value = "/done-shop",method = RequestMethod.GET)
    public String toDoneShopPage(HttpServletRequest request,Model model){
		LOGGER.info("进入完善商铺信息页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		Shop shop = null;
		if(user.getShopId() != null){
			shop = shopService.selectByPrimaryKey(user.getShopId());
		}
		if(shop == null){
			shop = new Shop();
			shop.setSalerId(user.getId());
		}
		model.addAttribute("shop", shop);
		model.addAttribute("user", user);
		return "/admin/app/done_shop";
    }
	@RequestMapping(value = "/done-user",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updateDoneUser(HttpServletRequest request,@RequestBody User user){
		LOGGER.info("进入完善用户信息页面");
		User userInfo = (User)request.getAttribute(CommonConstants.USER_INFO);
		userInfo.setStatus(11);
		userInfo.setName(user.getName());
		userInfo.setWeixin(user.getWeixin());
		userInfo.setWeixinQrCode(user.getWeixinQrCode());
		userInfo.setZhifubao(user.getZhifubao());
		userInfo.setZhifubaoQrCode(user.getZhifubaoQrCode());
		userInfo.setQq(user.getQq());
		userInfo.setSex(user.getSex());
		userInfo.setIdCard(user.getIdCard());
		userInfo.setAddress(user.getAddress());
		userInfo.setEmail(user.getEmail());
		//密码不能从这里改
		userInfo.setPwd(null);
		userService.updateByPrimaryKeySelective(userInfo);
        return new AjaxResult(true,"个人信息完善成功!");
    }
	
	@RequestMapping(value = "/done-shop",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updateDoneShop(HttpServletRequest request,@RequestBody Shop shop){
		LOGGER.info("进入完善商铺信息页面");
		try{
			User user = (User)request.getAttribute(CommonConstants.USER_INFO);
			shop.setSalerId(user.getId());
			user.setStatus(20);
			if(shop.getCode() == null || "".equals(shop.getCode().trim())){
				return new AjaxResult(false,"亲,店铺编码不能是空,不能包含空格!");
			}else if("admin".equals(shop.getCode().trim())){
				return new AjaxResult(false,"亲,店铺编码不能admin系统关键字!");
			}
			ServiceResult serviceResult = shopService.insert(user,shop);
			return new AjaxResult(serviceResult);
		}catch(Exception ex){
			return new AjaxResult(false,"店铺信息完善失败,请刷新后重试或联系管理员!");
		}
    }
}
