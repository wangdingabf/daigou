package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.Pay;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.OrderService;
import com.daigou.service.PayService;

@Controller("adminPayController")
@RequestMapping(value = "/admin")
public class PayController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PayController.class);

	@Autowired
	private PayService payService;
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/pay/{orderId}/add/{payMethod}",method = RequestMethod.GET)
    public String toPayPage(HttpServletRequest request,Model model,
    		@PathVariable("orderId")Long orderId,
    		@PathVariable("payMethod")String payMethod){
		LOGGER.info("进入支付记录页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		
		Order order = orderService.selectByPrimaryKey(orderId);
		model.addAttribute("order", order);
		
		Pay pay = new Pay();
		pay.setPayMethod(payMethod);
		pay.setPayMethodName(pay.getPayMethod());
		pay.setOrderId(orderId);
		pay.setPayMoney(order.getNoPayAmount());
		pay.setBuyerCode(order.getBuyerCode());
		pay.setBuyerName(order.getBuyerName());
		model.addAttribute("pay", pay);
		return "/admin/app/pay_add";
    }
	@RequestMapping(value = "/pay/{id}/details",method = RequestMethod.GET)
    public String toProductDetailsPage(Model model,@PathVariable("id")Long id){
		LOGGER.info("进入支付详情{}",id);
		Pay pay = payService.selectByPrimaryKey(id);
		model.addAttribute("pay", pay);
        return "/admin/app/pay_details";
    }
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/pay",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult paySave(HttpServletRequest request,@RequestBody Pay pay){
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		pay.setShopId(shop.getId());
		pay.setCreatorId(shop.getSalerId());
		pay.setCreatedTime(new Date());
		//0,待支付,1,确认支付,2,支付成功
		pay.setStatus(2);
		try{
			ServiceResult serviceResult = payService.insertPayAndUpdateOrder(pay);
			return new AjaxResult(serviceResult);
		}catch(Exception ex){
			return new AjaxResult(false,ex.getMessage());
		}
    }
	/**
	 * 状态修改
	 * @return
	 */
	@RequestMapping(value = "/pay/pay-status-update",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult payStatusUpdate(HttpServletRequest request,@RequestBody Pay pay){
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		pay.setModifierId(shop.getSalerId());
		pay.setModifiedTime(new Date());
		//0,待支付,1,确认支付,2,支付成功
		try{
			ServiceResult serviceResult = payService.insertPayAndUpdateOrder(pay);
			if(pay.getStatus().equals(2)){
				serviceResult.setMessage("支付状态更新成功，店主已确认收钱！");
			}else if(pay.getStatus().equals(0)){
				serviceResult.setMessage("支付状态更新成功，店主未收到收钱，请您及时与客户联系！");
			}
			return new AjaxResult(serviceResult);
		}catch(Exception ex){
			return new AjaxResult(false,ex.getMessage());
		}
    }
	@RequestMapping(value = "/pay/{orderId}/list",method = RequestMethod.GET)
    public String payList(HttpServletRequest request,Model model,@PathVariable("orderId")Long orderId){
		LOGGER.info("进入支付列表页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Pay> payList = payService.selectByOrderId(shop.getId(),orderId);
		model.addAttribute("payList", payList);
		model.addAttribute("orderId", orderId);
		return "/admin/app/pay_list";
    }
}
