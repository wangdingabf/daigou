package com.daigou.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.daigou.model.Trip;

public interface TripMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Trip record);

    int insertSelective(Trip record);

    Trip selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Trip record);
    
    List<Trip> selectByShopId(@Param("shopId")Long shopId);

    int updateByPrimaryKey(Trip record);
}