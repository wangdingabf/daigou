package com.daigou.tools;

import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.daigou.model.Shop;
import com.daigou.model.User;

public class CacheManagerTool {
	
	public static final String SHOP_CACHE_BY_CODE = "SHOP_CACHE_BY_CODE";
	public static final String SHOP_CACHE_BY_ID = "SHOP_CACHE_BY_ID";
	
	public static final String MANAGER_REGISTE = "MANAGER_REGISTE";
	public static final String CUSTOMER_REGISTE = "CUSTOMER_REGISTE";
	
	/**
	 * ehcache 缓存插件
	 */
	private EhCacheCacheManager cacheManager;
	
	public EhCacheCacheManager getCacheManager() {
		return cacheManager;
	}
	public void setCacheManager(EhCacheCacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	/**
	 * 设置user缓存
	 * @param key
	 * @param value
	 */
	public void putUserCache(String key,User value){
		Cache objcache = cacheManager.getCache("userCache");
		objcache.put(key,value);
	}
	/**
	 * 获取user缓存
	 * @param key
	 * @param value
	 */
	public User getUserCache(String key){
		Cache sample = cacheManager.getCache("userCache");
		return sample.get(key,User.class);
	}
	/**
	 * 设置user缓存
	 * @param key
	 * @param value
	 */
	public void putVerifyCodeCache(String type,String key,Integer value){
		Cache objcache = null;
		if(MANAGER_REGISTE.equals(type)){
			objcache = cacheManager.getCache("managerRegisterCache");
		}else{
			objcache = cacheManager.getCache("customerRegisterCache");
		}
		objcache.put(key,value);
	}
	/**
	 * 获取user缓存
	 * @param key
	 * @param value
	 */
	public Integer getVerifyCodeCache(String type,String key){
		Cache objcache = null;
		if(MANAGER_REGISTE.equals(type)){
			objcache = cacheManager.getCache("managerRegisterCache");
		}else{
			objcache = cacheManager.getCache("customerRegisterCache");
		}
		return objcache.get(key,Integer.class);
	}
	/**
	 * 删除系统缓存
	 * @param key
	 * @param value
	 */
	public void removeUserCache(String key){
		Cache sample = cacheManager.getCache("userCache");
		sample.put(key, null);
	}
	/**
	 * 通过key获取shopMap
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Shop getShopByKey(String keyType,String key){
		Cache sample = cacheManager.getCache("shopCache");
		Map<String,Object> shopMap = sample.get(keyType,Map.class);
		if(shopMap == null){
			return null;
		}else{
			Shop shop = (Shop)shopMap.get(key);
			return shop;
		}
	}
	/**
	 * 通过key获取shopMap
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> getShopMapByKey(String keyType){
		Cache sample = cacheManager.getCache("shopCache");
		Map<String,Object> shopMap = sample.get(keyType,Map.class);
		if(shopMap == null){
			shopMap = new HashMap<String,Object>();
			sample.put(keyType, shopMap);
			return shopMap;
		}else{
			return shopMap;
		}
	}
	
	/**
	 * 设置shopMap
	 * @param key
	 * @param value
	 */
	public void putShopCache(String keyType,String key,Object value){
		Map<String,Object> shopMap = getShopMapByKey(keyType);
		shopMap.put(key,value);
	}
	
	/**
	 * 通过key移除Shop缓存
	 * @param key
	 */
	public void removeShopCache(String keyType,String key){
		Map<String,Object> shopMap = getShopMapByKey(keyType);
		shopMap.put(key,null);
	}
}
