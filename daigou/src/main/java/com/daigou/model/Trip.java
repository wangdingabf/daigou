package com.daigou.model;

import java.io.Serializable;
import java.util.Date;

import com.daigou.enums.TripStatusEnum;

public class Trip implements Serializable {
    private Long id;

    private Long shopId;

    private String shopName;

    private String targetAddress;

    private Date startDate;

    private Date endDate;

    private String tripPic;
    
    private String tripPic1;
    private String tripPic2;
    private String tripPic3;
    private String tripPic4;
    private String tripPic5;
    private String tripPic6;

    private String remark;
    /**
     * 0,新建
     * 1，准备出发
     * 2，采购中
     * 3，结束，返程
     */
    private Integer status;
    
    private String statusName;

    private Long creatorId;

    private Date createdTime;

    private Long modifierId;

    private Date modifiedTime;

    private Integer dr = 1;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }

    public String getTargetAddress() {
        return targetAddress;
    }

    public void setTargetAddress(String targetAddress) {
        this.targetAddress = targetAddress == null ? null : targetAddress.trim();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTripPic() {
        return tripPic;
    }

    public void setTripPic(String tripPic) {
        this.tripPic = tripPic == null ? null : tripPic.trim();
    }

    public String getTripPic1() {
		return tripPic1;
	}

	public void setTripPic1(String tripPic1) {
		this.tripPic1 = tripPic1;
	}

	public String getTripPic2() {
		return tripPic2;
	}

	public void setTripPic2(String tripPic2) {
		this.tripPic2 = tripPic2;
	}

	public String getTripPic3() {
		return tripPic3;
	}

	public void setTripPic3(String tripPic3) {
		this.tripPic3 = tripPic3;
	}

	public String getTripPic4() {
		return tripPic4;
	}

	public void setTripPic4(String tripPic4) {
		this.tripPic4 = tripPic4;
	}

	public String getTripPic5() {
		return tripPic5;
	}

	public void setTripPic5(String tripPic5) {
		this.tripPic5 = tripPic5;
	}

	public String getTripPic6() {
		return tripPic6;
	}

	public void setTripPic6(String tripPic6) {
		this.tripPic6 = tripPic6;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusName() {
		return statusName;
	}

	public void setStatusName(Integer status) {
		this.statusName = TripStatusEnum.valueOf(status).getText();
	}

	public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }
}