package com.daigou.model;

import java.io.Serializable;
import java.util.Date;

import com.daigou.tools.AuthSqlBuilder;

public class ShopCustomer implements Serializable {
    private Long id;

    private Long shopId;

    private Long customerId;

    private String customerName;

    private Integer customerLevel = 0;

    private Integer customerDiscount = 100;

    private Long mobile;

    private Long businessId;

    private String businessName="";

    private Long creatorId = AuthSqlBuilder.ALL_AUTH_TAG;

    private Date createdTime;

    private Long modifierId;

    private Date modifiedTime;

    private Integer dr = 1;
    
    //公共客户表信息
    
    private String loginName;

    private String linkName;

    private String pwd;

    private Integer type = -1;

    private Integer status;

    private String email;

    private String weixin;

    private String zhifubao;

    private String qq;

    private String weixinQrCode;

    private String zhifubaoQrCode;

    private String qqQrCode;

    private String idCard;

    private Integer sex;

    private String address;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    public Integer getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(Integer customerLevel) {
        this.customerLevel = customerLevel;
    }

    public Integer getCustomerDiscount() {
        return customerDiscount;
    }

    public void setCustomerDiscount(Integer customerDiscount) {
        this.customerDiscount = customerDiscount;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getZhifubao() {
		return zhifubao;
	}

	public void setZhifubao(String zhifubao) {
		this.zhifubao = zhifubao;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixinQrCode() {
		return weixinQrCode;
	}

	public void setWeixinQrCode(String weixinQrCode) {
		this.weixinQrCode = weixinQrCode;
	}

	public String getZhifubaoQrCode() {
		return zhifubaoQrCode;
	}

	public void setZhifubaoQrCode(String zhifubaoQrCode) {
		this.zhifubaoQrCode = zhifubaoQrCode;
	}

	public String getQqQrCode() {
		return qqQrCode;
	}

	public void setQqQrCode(String qqQrCode) {
		this.qqQrCode = qqQrCode;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
    
}