package com.daigou.service;
import java.util.List;

import com.daigou.dto.AjaxResult;
import com.daigou.dto.ServiceResult;
import com.daigou.dto.UserQuery;
import com.daigou.model.User;

 

/**  
 * @Title: UserService.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
public interface UserService{ 
	
	/**
	 * 为订单写的插入用户信息
	 * @param orderUser
	 * @return
	 */
	public ServiceResult insertOrUpdate4Order444(User orderUser);
	/**
	 * 登录服务
	 * @param loginName
	 * @param pwd
	 * @param verifyCode
	 * @return
	 */
	public User login(String loginName,String pwd);
	
	/**
	 * 查找所有用户
	 * @param userinfo
	 * @return
	 */
	public User selectAllUser(User userinfo);
	/**
	 * 通过主键查找用户信息
	 * @param uId
	 * @return
	 */
	public User selectByPrimaryKey(Long id);
	/**
	 * 注册
	 * @param userInfo
	 * @return
	 */
	public ServiceResult register(User userInfo);
	/**
	 * 员工的新增和保存
	 * @param employee
	 * @return
	 */
	public ServiceResult insertOrUpdateEmployee(User employee);
	
	/**
	 * 通过用户名查找用户
	 * @param loginName
	 * @return
	 */
	public User selectByLoingName(String loginName);
	
	/**
	 * 通过email查找用户id
	 * @param uId
	 * @return
    */
	public User selectByEmail(String email);
	/**
	 * 忘记密码
	 * @param email
	 * @return
	 */
	public AjaxResult forgetPassword(String email);
	
	public int updatePasswordByEmail(String email,String pwd);
	
	public int updatePasswordById(Long id,String pwd);
	
	public int updateByPrimaryKeySelective(User userInfo);
	
	/**
	 * 我所有的员工
	 * @param userId
	 * @return
	 */
	public List<User> selectMyEmployee(UserQuery userQuery);
	/**
	 * 通过shopId查找管理员
	 * 
	 * @param shopId
	 * @return
	 */
	public User selectAdminUser(Long shopId);
}  