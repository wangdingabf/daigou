package com.daigou.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.TripMapper;
import com.daigou.model.Trip;
import com.daigou.service.TripService;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: TripServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("tripService")  
public class TripServiceImpl implements TripService {
	@Autowired  
	protected TripMapper tripMapper;
	
	public Trip selectByPrimaryKey(Long id){
		Trip trip = tripMapper.selectByPrimaryKey(id);
		trip.setStatusName(trip.getStatus());
		return trip;
	}
	
	public int insert(Trip record){
		record.setId(GenerateUUID.generateUUID());
		return tripMapper.insert(record);
	}

	public int updateByPrimaryKeySelective(Trip record){
		return tripMapper.updateByPrimaryKeySelective(record);
	}
	
	public List<Trip> selectByShopId(Long shopId){
		List<Trip> tripList = tripMapper.selectByShopId(shopId);
		for(Trip trip:tripList){
			trip.setStatusName(trip.getStatus());
		}
		return tripList;
	}
}  