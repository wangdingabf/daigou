package com.daigou.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.CustomerMapper;
import com.daigou.dao.ShopCustomerMapper;
import com.daigou.dto.CustomerQuery;
import com.daigou.dto.PageInfo;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Customer;
import com.daigou.model.Shop;
import com.daigou.model.ShopCustomer;
import com.daigou.service.CustomerService;
import com.daigou.tools.GenerateUUID;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private ShopCustomerMapper shopCustomerMapper;
	
	@Autowired
	private CustomerMapper customerMapper;
	
	@Override
	public ShopCustomer selectByPrimaryKey(Long id){
		return shopCustomerMapper.selectByPrimaryKey(id);
	}
	/**
	 * left join 联表查询
	 */
	@Override
	public ShopCustomer selectByShopIdAndMobile(Long shopId,Long mobile){
		return shopCustomerMapper.selectByShopIdAndMobile(shopId, mobile);
	}
	
	@Override
	public int deleteByPrimaryKey(Long id){
		return shopCustomerMapper.deleteByPrimaryKey(id);
	}
	/**
	 * 保存商铺客户信息
	 * @param shop
	 * @param customer
	 * @return
	 */
	@Override
	public int saveCustomer(Shop shop,Customer customer){
		ShopCustomer shopCustomer = new ShopCustomer();
		shopCustomerMapper.insertSelective(shopCustomer);
		customerMapper.insertSelective(customer);
		return 1;
	}
	public int updateCustomer(Shop shop,Customer customer){
		shopCustomerMapper.updateByPrimaryKeySelective(null);
		customerMapper.updateByPrimaryKeySelective(customer);
		return 1;
	}
	
	public PageInfo<ShopCustomer> selectShopCustomer(CustomerQuery customerQuery){
		PageInfo<ShopCustomer> pageInfo = new PageInfo<ShopCustomer>();
		List<ShopCustomer> shopCustomerList = shopCustomerMapper.selectShopCustomerByRole(customerQuery);
		int total = shopCustomerMapper.selectCountByRole(customerQuery);
		pageInfo.setRows(shopCustomerList);
		pageInfo.setTotal(total);
		return pageInfo;
	}
	public ServiceResult insertOrUpdate(ShopCustomer shopCustomer){
		Customer customerDB = customerMapper.selectByMobile(shopCustomer.getMobile());
		if(customerDB == null){
			customerDB = new Customer();
			BeanUtils.copyProperties(shopCustomer, customerDB);
			customerDB.setId(GenerateUUID.generateUUID());
			customerDB.setLoginName(shopCustomer.getMobile().toString());
			customerMapper.insertSelective(customerDB);
			if(shopCustomer.getShopId() != null){
				shopCustomer.setId(GenerateUUID.generateUUID());
				shopCustomer.setCustomerId(customerDB.getId());
				shopCustomerMapper.insertSelective(shopCustomer);
			}
		}else{
			Customer customerNew = new Customer();
			BeanUtils.copyProperties(shopCustomer, customerNew);
			customerNew.setId(customerDB.getId());
			customerMapper.updateByPrimaryKeySelective(customerNew);
			if(shopCustomer.getShopId() != null){
				ShopCustomer shopCustomerDB = shopCustomerMapper.selectByShopIdAndCustomerId(shopCustomer.getShopId(),customerDB.getId());
				//实例化
				if(shopCustomerDB == null){
					shopCustomer.setId(GenerateUUID.generateUUID());
					shopCustomer.setCustomerId(customerDB.getId());
					shopCustomerMapper.insertSelective(shopCustomer);
				}else{
					shopCustomer.setCustomerId(customerDB.getId());
					shopCustomerMapper.updateByPrimaryKeySelective(shopCustomer);
				}
			}
		}
		return new ServiceResult(true,"客户创建成功！",shopCustomer);
	}
	
	public Customer selectByMobile(Long mobile){
		return customerMapper.selectByMobile(mobile);
	}
}
