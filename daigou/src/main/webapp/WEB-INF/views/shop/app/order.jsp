<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>订单</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/admin/app/js/layer/skin/layer.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
	<script> var baselocation='<%=basePath%>';</script>
</head>
<body>
	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div class="aui-car-white-Typeface">填写订单</div>
			</div>
		</div>
		<a href="javascript:;" class="aui-header-item-icon select" style="min-width:0;">
			<i class="aui-icon aui-icon-share-pd selectVal" onselectstart="return false"></i>
			<div class="aui-header-drop-down selectList">
				<div class="aui-header-drop-down-san"></div>
				<div class="">
					<p class="" onclick="location='<%=basePath%>/${shop.code }'">首页</p>
				</div>
			</div>
		</a>
	</header>
	<section class="aui-address-content">
		<div class="aui-prompt"><i class="aui-icon aui-prompt-sm"></i>请注意核对您的收获信息，并做好收货准备。</div>
		<div class="aui-address-box">
			<div class="aui-address-box-list">
				<a id="a-user-info-href" href="<%=basePath%>/user/to-user-info/${product.id }" class="aui-address-box-default">
					<ul>
						<li>
							<strong id="strong-name">请输入收货人名称</strong>
						</li>
						<li>
							<strong id="strong-mobile">手机号：请输入收货人手机</strong>
						</li>
						<li>
							<strong id="strong-weixin">微信号：请输入微信号</strong>
						</li>
						<li>
							<strong id="strong-email">邮箱：空空如野</strong>
						</li>
						<li id="li-address">
							<i class="aui-icon aui-icon-address"></i>
							请输入收货人地址信息
						</li>
					</ul>
				</a>
			</div>
		</div>
		<div class="aui-dri"></div>
		<!-- 订单购买用户信息 -->
		<input id="buyerCode" type="hidden" value="${order.buyerCode }">
		<input id="buyerName" type="hidden" value="${order.buyerName }">
		<input id="buyerWeixin" type="hidden" value="${order.buyerWeixin }">
		<input id="address" type="hidden" value="${order.address }">
		<!-- 订单信息 -->
		<input id="productId" type="hidden" value="${product.id }">
		<input id="shopId" type="hidden" value="${product.shopId }">
		<input id="productName" type="hidden" value="${order.productName }">
		<input id="price" type="hidden" value="${order.price }">
		<input id="spec" type="hidden" value="${order.spec }">
		<input id="cate" type="hidden" value="${order.cate }">
		<input id="brand" type="hidden" value="${order.brand }">
		<input id="productionPlace" type="hidden" value="${order.productionPlace }">
		<input id="status" type="hidden" value="${order.status }">
		<input id="payStatus" type="hidden" value="${order.payStatus }">
		<div class="aui-list-product-float-item">
			<div class="aui-car-box-list">
				<ul>
					<li>
						<div class="aui-car-box-list-item">
							<div class="aui-car-box-list-img">
								<a href="<%=basePath%>/product/${product.id }">
									<div class="picMainImagecontain">
										<img id="picMainImage" src="${order.picMain }" alt="">
									</div>
								</a>
							</div>
							<div class="aui-car-box-list-text">
								<h4>
									<a href="<%=basePath%>/product/${product.id }">${order.productName }</a>
								</h4>
								<div class="aui-car-box-list-text-brief">
									<c:if test=""><span>规格:${order.spec }</span></c:if>
									<c:if test=""><span>品牌:${order.brand }</span></c:if>
									<c:if test=""><span>产地:${order.productionPlace }</span></c:if>
								</div>
								<div class="aui-car-box-list-text-price">
									<div class="aui-car-box-list-text-pri">
										￥<b class="price">${order.price }</b>
									</div>
									<div class="aui-car-box-list-text-arithmetic">
										<a href="javascript:;" class="minus">-</a>
										<span id="productNum" class="num">1</span>
										<a href="javascript:;" class="plus">+</a>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="aui-address-well">
			<div class="aui-dri"></div>
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">
					<h3>商品总金额</h3>
					<p>免运费</p>
				</div>
				<div class="aui-address-cell-ft">
					<span id="span-total-amount" class="aui-red">￥${order.totalAmount }</span><br>
				</div>
			</a>
			<div class="aui-dri"></div>
			<textarea id="remark" class="aui-Address-box-text" placeholder="如有特殊需求，请填写备注信息" rows="3">${order.remark }</textarea>
			<div class="aui-dri"></div>
		</div>
		<div class="aui-payment-bar">
			<div class="shop-total">
				<span class="aui-red aui-size">实付款: <em id="span-em-total-amount">￥${order.price }</em></span>
			</div>
			<a id="submitOrder" class="settlement">提交订单</a>
			<input id="buttonFlag" type="hidden">
		</div>
	</section>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.cookie.js"></script>
	<script rel="script" src="<%=basePath%>/static/admin/app/js/layer/layer.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-down.js"></script>
	<script rel="script" src="<%=basePath%>/static/admin/app/js/common.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/order.js"></script>
	<script type="text/javascript">
	window.onload=function () {
	    //显示商品图
	    show_pic_view('90','90','picMainImage');
	}
	</script>
</body>
</html>