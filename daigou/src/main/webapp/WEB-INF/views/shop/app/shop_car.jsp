<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>购物车</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="icon" type="image/x-icon" href="favicon.ico">
	<link href="iTunesArtwork@2x.png" sizes="114x114" rel="apple-touch-icon-precomposed">
</head>
<body>

	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div class="aui-car-white-Typeface">购物车</div>
			</div>
		</div>
		<a href="javascript:;" class="aui-header-item-icon select" style="min-width:0;">
			<i class="aui-icon aui-icon-share-pd selectVal" onselectstart="return false"></i>
			<div class="aui-header-drop-down selectList">
				<div class="aui-header-drop-down-san"></div>
				<div class="">
					<p class="" onclick="location='index.html'">首页</p>
					<c:if test="${not empty loginName }">
					<p class="" onclick="location='<%=basePath%>/${shop.code }/${loginName}/order/list'">我的订单</p>
					</c:if>
					<p class="" onclick="location='index.html'">帮助</p>
				</div>
			</div>
		</a>
	</header>
	<section class="aui-car-content">
		<div class="aui-car-box">
			<div class="aui-car-box-name">
				<input type="checkbox" class="check goods-check shopCheck">
				<h3>
					<a href="#"></a>
				</h3>
				<div class="aui-car-coupons">
				</div>
			</div>
			<div class="aui-car-box-list">
				<ul>
					<c:forEach var="product" items="${productList}">
					<li>
						<div class="aui-car-box-list-item">
							<input type="checkbox" class="check goods-check goodsCheck">
							<div class="aui-car-box-list-img">
								<a href="<%=basePath%>/product/${product.id }">
									<div class="picMainImagecontain">
										<img id="picMainImage" src="${order.picMain }" alt="">
									</div>
								</a>
							</div>
							<div class="aui-car-box-list-text">
								<h4>
									<a href="ui-product.html">${product.name }</a>
								</h4>
								<div class="aui-car-box-list-text-brief">
									<span>${product.spec }</span>
									<span>${product.brand }</span>
									<span>${product.cate }</span>
								</div>
								<div class="aui-car-box-list-text-price">
									<div class="aui-car-box-list-text-pri">
										￥<b class="price">${product.salePrice }</b>
									</div>
									<div class="aui-car-box-list-text-arithmetic">
										<a href="javascript:;" class="minus">-</a>
										<span class="num">1</span>
										<a href="javascript:;" class="plus">+</a>
									</div>
								</div>
							</div>

						</div>
					</li>
					</c:forEach>
				</ul>
			</div>
			<div class="aui-shopPrice">
				本店总计：￥
				<span id="span-total-amount" class="aui-total-amount ShopTotal">00.00</span>
			</div>
		</div>
	</section>
	<div class="aui-payment-bar">
		<div class="all-checkbox"><input type="checkbox" class="check goods-check" id="AllCheck">全选</div>
		<div class="shop-total">
			<strong>合计：<i class="total" id="i-all-total-amount">00.00</i></strong>
		</div>
		<a href="<%=basePath%>/order/product/12345678/go-buy" class="settlement">结算</a>
	</div>
	<script src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-car.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-down.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/shop_car.js"></script>
	<script type="text/javascript">
	window.onload=function () {
	    //显示商品图
	    show_pic_view('90','90','picMainImage');
	}
	</script>
</body>
</html>