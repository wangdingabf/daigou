<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>${shop.name }</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/share.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
</head>
<body style="background:#fc0f42">
	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="#" class="aui-header-item">
			<!--<i class="aui-icon aui-icon-back-white" id="scrollSearchI" style="display:block"></i>-->
			<div id="scrollSearchDiv">
				<img src="${shop.logo }" alt="">
			</div>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="">

			</div>
		</div>
		<a href="javascript:;" class="aui-header-item-icon select"    style="min-width:0;">
			<i class="aui-icon aui-icon-share-pd selectVal" onselectstart="return false"></i>
			<div class="aui-header-drop-down selectList">
				<div class="aui-header-drop-down-san"></div>
				<div class="">
					<p class="" onclick="location='<%=basePath%>/${shop.code }'">首页</p>
					<c:if test="${not empty loginName }">
					<p class="" onclick="location='<%=basePath%>/${shop.code }/${loginName}/order/list'">我的订单</p>
					</c:if>
					<p class="" id="share_btn_p">分享</p>
				</div>
			</div>
		</a>
	</header>

	<section class="aui-me-content">
		<div class="aui-me-content-box">
			<div class="aui-me-content-info"></div>
			<div class="aui-me-content-list">
				<div class="aui-me-content-item">
					<div class="aui-me-content-item-head">
						<div class="aui-me-content-item-img">
							<img src="${shop.logo }" alt="">
						</div>
						<div class="aui-me-content-item-title">${shop.name }</div>
					</div>
					<div class="aui-me-content-item-text">
						${shop.description }
					</div>
				</div>
				<div class="aui-me-content-card">
					<h3><i class="aui-icon aui-card-me"></i>平台担保授信</h3>
				</div>
			</div>
		</div>
		<div class="aui-me-content-order">
		</div>
		<section class="aui-grid-content">
			<div class="aui-me-content-order">
			</div>
			<div class="aui-grid-row">
			</div>
			<div class="aui-recommend">
				店主信息
			</div>
			<section class="aui-list-product">
				<div class="aui-list-product-box">
					<a class="aui-list-product-item">
						<div class="aui-list-product-item-img">
							<div class="qr_code_contain">
								<img id="qr_code_weixin" src="${user.weixinQrCode }"/>
							</div>
						</div>
						<div class="aui-list-product-item-text">
							<h3>微信二维码</h3>
							<div class="aui-list-product-mes-box">
								<div class="aui-comment"></div>
							</div>
						</div>
					</a>
					<a class="aui-list-product-item">
						<div class="aui-list-product-item-img">
							<div class="qr_code_contain">
								<img id="qr_code_zhifubao" src="${user.zhifubaoQrCode }"/>
							</div>
						</div>
						<div class="aui-list-product-item-text">
							<h3>支付宝二维码</h3>
							<div class="aui-list-product-mes-box">
								<div class="aui-comment"></div>
							</div>
						</div>
					</a>
				</div>
			</section>
			<div class="aui-recommend">
				以上是店主信息，您可加微信和支付宝好友！
			</div>
		</section>
		<div class="aui-product-pages">
			<div class="aui-product-pages-title">长按二维码保存到本地!赶快分享吧！
			</div>
			<div id="advertQrCodeDiv" class="aui-product-pages-img">
			</div>
			<div style="position: relative;margin:0 auto;padding:1px 1px;border:1px solid;width:204px;height:204px;">
				<img id="advertQrCodeImage" alt="">
			</div>
		</div>
	</section>
	<%@ include file="share.jsp" %>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-down.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/share.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.qrcode.min.js"></script>
	<script type="text/javascript">
	window.onload=function () {
		var browser_width = $(document.body).width();
	    var widthSize = browser_width*0.35;//正方形容器的大小的宽=高
	    var heightSize = widthSize;//正方形容器的大小宽=高
	    //显示商品图
	    show_pic_view_all(widthSize,heightSize,'qr_code_');
	}
	</script>
	<script type="text/javascript">
        $(function () {
        	var qrcode = $('#advertQrCodeDiv').qrcode({width:200,height:200,correctLevel:0,text:'<%=basePath%>/${shop.code }'}).hide();
        	var canvas=qrcode.find('canvas').get(0);  
        	$('#advertQrCodeImage').attr('src',canvas.toDataURL('image/jpg'));
        	//绑定滚动条事件
            $(window).bind("scroll", function () {
                var sTop = $(window).scrollTop();
                var sTop = parseInt(sTop);
                if (sTop >= 44) {
                    if (!$("#scrollSearchDiv").is(":visible")) {
                        try {
                            $("#scrollSearchDiv").slideDown();
                        } catch (e) {
                            $("#scrollSearchDiv").show();
                        }
                    }
                }
                else {
                    if ($("#scrollSearchDiv").is(":visible")) {
                        try {
                            $("#scrollSearchDiv").slideUp();
                        } catch (e) {
                            $("#scrollSearchDiv").hide();
                        }
                    }
                }
            });
        })
	</script>
</body>
</html>