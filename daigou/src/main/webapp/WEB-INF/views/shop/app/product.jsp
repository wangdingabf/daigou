<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>${product.name }</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/share.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
	<style>
		.m-button {
			padding: 0 0.24rem;
		}

		.btn-block {
			text-align: center;
			position: relative;
			border: none;
			pointer-events: auto;
			width: 100%;
			display: block;
			font-size: 1rem;
			height: 2.5rem;
			line-height: 2.5rem;
			margin-top: 0.5rem;
			border-radius: 3px;
		}

		.btn-primary {
			background-color: #04BE02;
			color: #FFF;
		}

		.btn-warning {
			background-color: #FFB400;
			color: #FFF;
		}
		.mask-black {
			background-color: rgba(0, 0, 0, 0.6);
			position: fixed;
			bottom: 0;
			right: 0;
			left: 0;
			top: 0;
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
			z-index:999;
		}

		.m-actionsheet {
			text-align: center;
			position: fixed;
			bottom: 0;
			left: 0;
			width: 100%;
			z-index: 1000;
			background-color: #EFEFF4;
			-webkit-transform: translate(0, 100%);
			transform: translate(0, 100%);
			-webkit-transition: -webkit-transform .3s;
			transition: -webkit-transform .3s;
			transition: transform .3s;
			transition: transform .3s, -webkit-transform .3s;
		}
		.actionsheet-toggle {
			-webkit-transform: translate(0, 0);
			transform: translate(0, 0);
		}
		.actionsheet-item {
			display: block;
			position: relative;
			font-size: 0.8rem;
			color: #555;
			height: 2rem;
			line-height: 2rem;
			background-color: #FFF;
		}
		.actionsheet-item:after {
			content: '';
			position: absolute;
			z-index: 2;
			bottom: 0;
			left: 0;
			width: 100%;
			height: 1px;
			border-bottom: 1px solid #D9D9D9;
			-webkit-transform: scaleY(0.5);
			transform: scaleY(0.5);
			-webkit-transform-origin: 0 100%;
			transform-origin: 0 100%;
		}
		.actionsheet-action {
			display: block;
			margin-top: .15rem;
			font-size: 0.8rem;
			color: #555;
			height:3rem;
			line-height: 3rem;
			background-color: #FFF;
			position:absolute;
			top:10px;
			right:0;
		}
		.shop-kefu-div{ width:200px; height:220px;background:#D8D8D8; position:fixed; left:5px; bottom:55px;display:none;z-index:1000;}
		.shop-kefu-div .close{ width:30px; height:22px; line-height:22px;display:block; float:right;z-index:1001}
	</style>
</head>
<body>
	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div id="scrollSearchPro">
					<span class="current"></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
		<a href="javascript:;" class="aui-header-item-icon select"    style="min-width:0;">
			<i class="aui-icon aui-icon-share-pd selectVal" onselectstart="return false"></i>
			<div class="aui-header-drop-down selectList">
				<div class="aui-header-drop-down-san"></div>
				<div class="">
					<p class="" onclick="location='<%=basePath%>/${shop.code }'">首页</p>
					<c:if test="${not empty loginName }">
					<p class="" onclick="location='<%=basePath%>/${shop.code }/${loginName}/order/list'">我的订单</p>
					</c:if>
					<p class="" id="share_btn_p">分享</p>
				</div>
			</div>
		</a>
	</header>
	<div class="aui-banner-content aui-fixed-top" data-aui-slider>
		<div class="aui-banner-wrapper">
			<div class="aui-banner-wrapper-item">
				<a href="#">
					<div class="scroll_contain">
						<img id="scroll_picMain" src="${product.picMain }"/>
					</div>
				</a>
			</div>
			<c:if test="${not empty product.pic1 }">
			<div class="aui-banner-wrapper-item">
				<a href="#">
					<div class="scroll_contain">
						<img id="scroll_pic1" src="${product.pic1 }"/>
					</div>
				</a>
			</div>
			</c:if>
			<c:if test="${not empty product.pic2 }">
			<div class="aui-banner-wrapper-item">
				<a href="#">
					<div class="scroll_contain">
						<img id="scroll_pic2" src="${product.pic2 }"/>
					</div>
				</a>
			</div>
			</c:if>
			<c:if test="${not empty product.pic3 }">
			<div class="aui-banner-wrapper-item">
				<a href="#">
					<div class="scroll_contain">
						<img id="scroll_pic3" src="${product.pic3 }"/>
					</div>
				</a>
			</div>
			</c:if>
		</div>
		<div class="aui-banner-pagination"></div>

	</div>
	<div class="aui-product-content">
		<div class="aui-real-price clearfix">
			<span>
				<i>￥</i>${product.salePrice }
			</span>
			<del><span class="aui-font-num">￥${product.marketPrice }</span></del>
			<div class="aui-settle-choice">
				<span>促销价</span>
			</div>
		</div>
		<div class="aui-product-title">
			<h2>
				${product.name }
			</h2>
			<p>
				${product.description }
			</p>
		</div>
		<div class="aui-product-boutique clearfix">
			<img src="<%=basePath%>/static/shop/app/img/icon/icon-usa.png" alt="">
			<span class="aui-product-tag-text">授信商家</span>
			<img src="<%=basePath%>/static/shop/app/img/icon/icon-sj.png" alt="">
			<span class="aui-product-tag-text">精选商家</span>
		</div>
		<div class="aui-product-strip">
			<img src="<%=basePath%>/static/shop/app/img/bg/ssy.jpg" alt="">
		</div>
		<div class="aui-product-coupon">
			<c:if test="${not empty product.spec }">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear" data-ydui-actionsheet="{target:'#actionSheet',closeElement:'#cancel'}">
				<div class="aui-address-cell-bd">规格</div>
				<div class="aui-address-cell-ft">${product.spec }</div>
			</a>
			</c:if>
			<c:if test="${not empty product.brand }">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">品牌</div>
				<div class="aui-address-cell-ft">
					${product.brand }
				</div>
			</a>
			</c:if>
			<c:if test="${not empty product.productionPlace }">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">产地</div>
				<div class="aui-address-cell-ft">${product.productionPlace }</div>
			</a>
			</c:if>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-evaluate">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">商品描述</div>
				<div class="aui-address-cell-ft">
					${product.description }
				</div>
			</a>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-evaluate">
			<a href="<%=basePath%>/${shop.code }" class="aui-address-cell aui-fl-arrow aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">
					<div class="clearfix">
						<div class="aui-product-shop-img">
							<div class="shop_logo_contain">
								<img id="shop_logo_" src="${shop.logo }" alt=""/>
							</div>
						</div>
						<div class="aui-product-shop-text">
							<h3>${shop.name }</h3>
							<p>精选商家  服务保障</p>
							<p>在售商品${onSaleCount }件</p>
						</div>
					</div>
				</div>
				<div class="aui-address-cell-ft">
					<span>进店看看</span>
				</div>
			</a>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-slide-box">
			<div class="aui-slide-list">
				<ul class="aui-slide-item-list">
					<c:forEach var="product" items="${product6List}">
					<li class="aui-slide-item-item">
						<a href="<%=basePath%>/product/${product.id }" class="v-link">
							<div class="v-img-contain">
								<img id="v-img-${product.id }" class="v-img" src="${product.picMain }">
							</div>
							<p class="aui-slide-item-title aui-slide-item-f-els">${product.name }</p>
							<p class="aui-slide-item-info">
								<span class="aui-slide-item-price">¥${product.salePrice }</span>&nbsp;&nbsp;<span class="aui-slide-item-mrk">¥${product.marketPrice }</span>
							</p>
						</a>
					</li>
					</c:forEach>
				</ul>
			</div>

		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-pages">
			<div class="aui-product-pages-title">
				<h2>图文详情</h2>
			</div>
			<div class="aui-product-pages-img">
				<c:if test="${not empty product.pic1 }">
				<img src="${product.pic1 }" alt="">
				</c:if>
				<c:if test="${not empty product.pic2 }">
				<img src="${product.pic2 }" alt="">
				</c:if>
				<c:if test="${not empty product.pic3 }">
				<img src="${product.pic3 }" alt="">
				</c:if>
			</div>
		</div>
		<div class="aui-recommend">
			<img src="<%=basePath%>/static/shop/app/img/bg/icon-tj3.jpg" alt="">
			<!--<h2>为你推荐</h2>-->
		</div>
		<section class="aui-list-product">
			<div class="aui-list-product-box">
				<c:forEach var="product" items="${productList}">
				<a href="<%=basePath%>/product/${product.id }" class="aui-list-product-item">
					<div class="aui-list-product-item-img">
						<div class="product_contain">
							<img id="product_${product.id }" src="${product.picMain }"/>
						</div>
					</div>
					<div class="aui-list-product-item-text">
						<h3>${product.name }</h3>
						<div class="aui-list-product-mes-box">
							<div>
							<span class="aui-list-product-item-price">
								<em>¥</em>
								${product.salePrice }
							</span>
								<span class="aui-list-product-item-del-price">
								¥${product.marketPrice }
							</span>
							</div>
							<div class="aui-comment"></div>
						</div>
					</div>
				</a>
				</c:forEach>
			</div>
		</section>

	</div>

	<footer class="aui-footer-product">
		<div class="aui-footer-product-fixed">
			<div class="aui-footer-product-concern-cart">
				<a id="kefuButton">
					<span class="aui-f-p-icon"><img src="<%=basePath%>/static/shop/app/img/icon/icon-kf.png" alt=""></span>
					<span class="aui-f-p-focus-info">客服</span>
				</a>
				<a id="share_btn_a">
					<span class="aui-f-p-icon"><img src="<%=basePath%>/static/shop/app/img/icon/icon-sc.png" alt=""></span>
					<span class="aui-f-p-focus-info">收藏</span>
				</a>
				<a href="<%=basePath%>/${shop.code }">
					<span class="aui-f-p-icon"><img src="<%=basePath%>/static/shop/app/img/icon/icon-dp.png" alt=""></span>
					<span class="aui-f-p-focus-info">店铺</span>
				</a>
			</div>
			<div class="aui-footer-product-action-list">
				<!-- <a href="<%=basePath%>/order/product/${product.id}/shop-car" class="yellow-color">加入购物车</a> -->
				<a href="<%=basePath%>/order/product/${product.id}/go-buy" class="red-color" style="width:100%">立即购买</a>
			</div>
		</div>
	</footer>
	<div class="shop-kefu-div">
		<div style="position: relative;top:2px;z-index:1001;width:200px;height:20px;">
			<a href="javascript:" class="close">关闭</a>
		</div>
		<div class="qr_code_contain" style="border:1px solid;top:-20px; ">
			<img id="qr_code_" alt="" src="${user.weixinQrCode }"/>
		</div>
		<div style="position: relative;top:-40px;margin:0 auto;border:1px;width:180px;height:10px;">
		长按图片识别二维码加店主好友
	</div>
	</div>
	<%@ include file="share.jsp" %>
	<script src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-down.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/share.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script type="text/javascript">
	window.onload=function() {
		var browser_width = $(document.body).width();
	    var widthSize = browser_width;//正方形容器的大小的宽=高
	    var heightSize = widthSize;//正方形容器的大小宽=高
	  	//显示商品轮播图
	    show_pic_view_all(widthSize,heightSize,'scroll_');
	    //显示商品图
	    show_pic_view_all(widthSize*0.35,heightSize*0.35,'product_');
	    show_pic_view('65','65','shop_logo_');
	    //
	    show_pic_view_all('80','80','v-img-');
	}
	</script>
	<script type="text/javascript">
        $(function () {
            //绑定客服
        	$('.shop-kefu-div .close').click(function(){
				$('.shop-kefu-div').hide();
        	});
        	$("#kefuButton").click(function(){
				$('.shop-kefu-div').show();
				show_pic_view('198','218','qr_code_');
         	});
        	
            //绑定滚动条事件
            $(window).bind("scroll", function () {
                var sTop = $(window).scrollTop();
                var sTop = parseInt(sTop);
                if (sTop >= 100) {
                    if (!$("#scrollSearchPro").is(":visible")) {
                        try {
                            $("#scrollSearchPro").slideDown();
                        } catch (e) {
                            $("#scrollSearchPro").show();
                        }
                    }
                }
                else {
                    if ($("#scrollSearchPro").is(":visible")) {
                        try {
                            $("#scrollSearchPro").slideUp();
                        } catch (e) {
                            $("#scrollSearchPro").hide();
                        }
                    }
                }
            });
        })
	</script>
</body>
</html>