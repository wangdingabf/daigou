<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>收银台</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/admin/app/js/layer/skin/layer.css">
	<link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/wechat.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
	<script> var baselocation='<%=basePath%>';</script>
</head>
<body>

	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div class="aui-car-white-Typeface">微信支付</div>
			</div>
		</div>
		<a href="#" class="aui-header-item-icon"   style="min-width:0">
		</a>
	</header>
	<div style="position: relative;top:60px;margin:0 auto;border:1px;width:180px;height:42.5px;">
		<img alt="" src="<%=basePath%>/static/shop/app/img/pay/weixinpay@2x.png" width="180px" height="42.5px"/>
	</div>
	<div style="position: relative;text-align:center;top:60px;margin:0 auto;border:1px;">
		扫一扫付款(元)
	</div>
	<div style="position: relative;color:red;font-size:30px;text-align:center;top:70px;margin:0 auto;border:1px;">
		${pay.payMoney }
	</div>
	<div class="qr_code_contain" style="top:80px;margin:0 auto;border:1px solid;">
		<img id="qr_code_" alt="" src="${shop.weixinQrCode }"/>
	</div>
	<div style="position: relative;top:87px;margin:0 auto;border:1px;width:180px;height:60px;">
		<img alt="" src="<%=basePath%>/static/shop/app/img/pay/wechat-explain.png" width="180px" height="60px"/>
	</div>
	<div style="position: relative;left:5px;top:100px;color:blue;font-size:15px;margin:0 auto;border:1px;width:180px;height:30px;">
		长按图片识别二维码付款
	</div>
	<div style="position: relative;left:5px;top:100px;color:red;margin:0 auto;border:1px;width:220px;height:30px;">
		请于30分钟内付款或加店主好友直接付款
	</div>
	<c:if test="${pay.status == 0 }">
		<div id="countDownPay" style="position: relative;left:5px;top:100px;font-size:15px;margin:0 auto;border:1px;width:220px;height:30px;">
			剩余支付时间：<span>00</span>时<span>10</span>分<span>00</span>秒
		</div>
		<div class="aui-out" style="position: relative;top:90px;">
			<a id="payConfirmBtn" class="red-color" style="color:#fff">支付完毕</a>
		</div>
	</c:if>
	<c:if test="${pay.status == 1}">
		<div style="position: relative;left:5px;top:100px;margin:0 auto;border:1px;width:240px;height:30px;">
			该订单已经支付成功，等待卖家确认金额！
		</div>
	</c:if>
	<c:if test="${pay.status == 2}">
		<div style="position: relative;left:5px;top:100px;margin:0 auto;border:1px;width:240px;height:30px;">
			该订单已经支付成功，且买家已确认收钱！
		</div>
	</c:if>
	<input id="id" type="hidden" value="${pay.id }">
	<input id="ts" type="hidden" value="${pay.ts }">
	<input id="status" type="hidden" value="${pay.status }">
	<input id="orderId" type="hidden" value="${pay.orderId }">
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.cookie.js"></script>
	<script rel="script" src="<%=basePath%>/static/admin/app/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/pay.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script type="text/javascript">
	window.onload=function() {
	    show_pic_view('176','176','qr_code_');
	    setCountDown();
        setInterval(function(){ setCountDown() },1000);
	}
	</script>
</body>
</html>