<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/claim.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/bootstrap.min.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/footer.js"></script>
    <title>商品管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
    	<a href="<%=basePath%>/admin/product" class="return"><i class="iconfont"></i>返回</a>
        <h2>商品草稿</h2>
    </div>
</header>
<article>
	<div class="row">
        <ul class="claim-title-type">
            <li class="col-xs-12">亲,您可以点击草稿记录,查看详情</li>
        </ul>
    </div>
    <div class="date-tab-type">
        <ul class="date-tab-cell">
            <li>商品ID</li>
            <li>商品名称</li>
            <li>规格</li>
            <li>价格</li>
        </ul>
    </div>
    <div class="details-box">
    	<c:forEach var="product" items="${productList}">
        <a href="<%=basePath%>/admin/product/${product.id }/details" class="details-link">
            <ul class="details-list">
                <li>${product.id }</li>
                <li>${product.name }</li>
                <li>${product.spec }</li>
                <li>${product.salePrice }</li>
            </ul>
        </a>
    	</c:forEach>
    </div>
</article>
</div>
</body>
</html>