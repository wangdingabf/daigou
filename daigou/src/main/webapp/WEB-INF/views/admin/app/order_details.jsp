<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/basic.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/bootstrap.min.css">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/layer/layer.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/static/admin/app/js/layer/skin/layer.css">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/order_details.js"></script>
    <script> var baselocation='<%=basePath%>/admin';</script>
    <title>订单管理</title>
</head>
<body>
<div  class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/order" class="return"><i class="iconfont"></i>返回</a>
        <h2>订单明细</h2>
        <a href="<%=basePath%>/admin/order/${order.id }" class="news-a-brn">编辑</a>
    </div>
</header>
<article>
	<input id="id" type="hidden" class="form-control"  readonly value="${order.id }">
	<input id="ts" type="hidden" class="form-control"  readonly value="${order.ts }">
    <div class="info-con">
        <h2 class="info-title-font-size" >订单号(<font color="red">${order.id }</font>)</h2>
        <ul>
            <li>
                <span>店铺名称：</span>
                <span>${order.shopName }</span>
            </li>
            <li>
                <span>商品名称：</span>
                <span>${order.productName }</span>
            </li>
            <li>
                <span>商品主图：</span>
               	<span>
               		<a href="${order.picMain }">
               			<img id="picMainImage" src="${order.picMain }" width="98" height="100"/>
               		</a>
               	</span>
            </li>
            <li>
                <span>单价：</span>
                <span>￥${order.price }</span>
            </li>
            <li>
                <span>数量：</span>
               <span>${order.num }</span>
            </li>
            <li>
                <span>订单总价：</span>
                <span>￥${order.orderAmount }</span>
            </li>
            <li>
                <span>折扣率：</span>
                <span>${order.customerDiscount }%</span>
            </li>
            <li>
                <span>折后总价：</span>
                <span>￥${order.totalAmount }&nbsp;&nbsp;&nbsp;(共优惠：￥<font color="red">${order.orderAmount - order.totalAmount }</font>)</span>
            </li>
            <li>
                <span>订单状态：</span>
               	<span>${order.statusName }</span>
               	<c:if test="${order.dr == 1 and order.status == 1}">
	        	<a id="confirmBnt" class="btn btn-info btn-sm">确认收单</a>
				</c:if>
				<c:if test="${order.dr == 1 and order.status == 2}">
	            <a id="sendBnt" class="btn btn-success btn-sm">已发货</a>
				</c:if>
				<c:if test="${order.dr == 1 and order.status == 3}">
	            <a id="receiveBnt" class="btn btn-primary btn-sm">已收货</a>
				</c:if>
				<c:if test="${order.dr == 1 and order.status == 4}">
	            <a id="overBnt" class="btn btn-success btn-sm">完毕</a>
				</c:if>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">附属信息</h2>
        <ul>
            <li>
                <span>商品规格：</span>
                <span>${order.spec }</span>
            </li>
            <li>
                <span>商品类别：</span>
                <span>${order.cate }</span>
            </li>
            <li>
                <span>商品品牌：</span>
                <span>${order.brand }</span>
            </li>
            <li>
                <span>商品产地：</span>
                <span>${order.productionPlace }</span>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">购买人信息</h2>
        <ul>
            <li>
                <span>购买人姓名：</span>
                <span>${order.buyerName }</span>
            </li>
            <li>
                <span>送货地址：</span>
                <span>${order.address }</span>
            </li>
            <li>
                <span>联系电话：</span>
                <span>${order.buyerCode }</span>
            </li>
            <li>
                <span>微信：</span>
                <span>${order.buyerWeixin }</span>
            </li>
            <li>
                <span>备注：</span>
                <span>${order.remark }</span>
            </li>
        </ul>
    </div>
    <div class="info-con">
        <h2 class="info-title-font-size">
        	支付信息(${order.payStatusName })
        </h2>
        <ul>
            <li>
                <a href="<%=basePath%>/admin/pay/${order.id}/list" class="odd-numbers">
                    <i class="iconfont"></i>
                    <span>
                    	支付记录(已支付：<font color="blue">¥${order.payAmount }</font>&nbsp;&nbsp;待支付：<font color="red">¥${order.noPayAmount }</font>)
                    </span>
                </a>
            </li>
        </ul>
        <div class="pay-item">
			<div id="aliPay" class="active" onclick="toPayPage('${order.id }','1')"><img src="<%=basePath%>/static/admin/app/images/alipay@2x.png" alt=""></div>
			<div id="wechatPay" class="active" onclick="toPayPage('${order.id }','2')"><img src="<%=basePath%>/static/admin/app/images/weixinpay@2x.png" alt=""></div>
			<div id="qitaPay" class="active" onclick="toPayPage('${order.id }','3')">其他</div>
        </div>
    </div>
</article>
    <article>
		<div class="info-con">
	        <h2 class="info-title-font-size">发货信息</h2>
	        <ul>
	            <li>
	                <span>快递公司：</span>
	                <span>${order.sendCompany }</span>
	            </li>
	            <li>
	                <span>发货单号：</span>
	                <span>${order.sendNo }</span>
	            </li>
	            <li>
	                <span>发货时间：</span>
	                <span><fmt:formatDate value="${order.sendTime }" pattern="yyyy-MM-dd"/></span>
	            </li>
	        </ul>
	    </div>
	</article>
	<div style="height: 100px"></div>
    <div class="new-brn-tab">
        <div class="new-tbl-type">
        	<c:if test="${order.dr == 1}">
        	<a id="refoundBnt" class="new-tbl-cell btn btn-primary">退款退货</a>
        	</c:if>
            <a id="cancelBnt" class="new-tbl-cell btn btn-warning">取消</a>
			<a id="deleteBnt" class="new-tbl-cell btn btn-submit">删除</a>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" charset="utf-8">
//支付
function toPayPage(orderId,payMethod){
	window.location.href=baselocation+'/pay/'+orderId+'/add/'+payMethod;
}
</script>
</html>