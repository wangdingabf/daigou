<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <link href="<%=basePath%>/static/admin/app/js/datapicker/datepicker3.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/datapicker/bootstrap-datepicker.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/datapicker/bootstrap-datepicker.zh-CN.js"></script>
    <!--引入 lrz 插件 用于压缩图片-->
    <script type="text/javascript" src="<%=basePath%>/static/admin/app/js/localResizeIMG/lrz.bundle.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/fileUpload.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/order_update.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/js/touchspin/jquery.bootstrap-touchspin.min.css">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <title>订单管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/order" class="return"><i class="iconfont"></i>返回</a>
        <h2>新增订单</h2>
        <a id="doMyMindBtn" class="news-a-brn">自定义订单</a>
    </div>
</header>
<section>
    <form role="form">
        <input id="productId" type="hidden" class="form-control"  readonly value="${order.productId }">
        <div class="form-box">
            <div id="productNameDiv" class="form-group">
            	<input id="productName" type="hidden" class="form-control"  readonly value="${order.productName }">
            	<ul class="new-details-list">
	                <label><em class="red">*</em>商品名称</label>
		            <li>
		                <a href="<%=basePath%>/admin/order-product-list" class="odd-numbers">
		                    <i class="iconfont"></i>
		                    <span>
		                    	<c:if test="${empty order.productName}">请选择下单商品</c:if>
		                    	<c:if test="${not empty order.productName}">${order.productName}</c:if>
		                    </span>
		                </a>
		            </li>
            	</ul>
            </div>
            <div class="form-group">
                <label>商品图片</label>
                <a class="upload-file-btn">
                    <i class="iconfont"></i>
                    <img id="picMainImage" src="${order.picMain }" width="98" height="100"/>
                    <input id="picMain" type="file" class="upload_file" onchange="ajaxFileUpload('/${user.loginName }/upload/order','picMain','picMainImage')"/>
                </a>
            </div>
            <div class="form-group">
                <label><em class="red">*</em>数量</label>
                <input id="num" type="text" class="touchspin1"  placeholder="请输入销售数量" value="${order.num }">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>单价</label>
                <input id="price" type="text" class="touchspin1"  placeholder="请输入商品单价" value="${order.price }">
            </div>
            <div class="form-group">
                <label>商品规格</label>
                <input id="spec" type="text" class="form-control"  placeholder="请输入商品规格" value="${order.spec }">
            </div>
            <div class="form-group">
                <label>商品类别</label>
                <input id="cate" type="text" class="form-control"  placeholder="请输入商品类别" value="${order.cate }">
            </div>
            <div class="form-group">
                <label>商品品牌</label>
                <input id="brand" type="text" class="form-control"  placeholder="请输入商品品牌" value="${order.brand }">
            </div>
            <div class="form-group">
                <label>商品产地</label>
                <input id="productionPlace" type="text" class="form-control"  placeholder="请输入商品产地" value="${order.productionPlace }">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>购买人电话</label>
                <input id="buyerCode" type="text" class="form-control" onkeyup="value=value.replace(/[^\d]/g,'')" ng-pattern="/[^a-zA-Z]/" placeholder="请输入电话" value="${order.buyerCode }">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>购买人姓名</label>
                <input id="buyerName" type="text" class="form-control"  placeholder="请输入姓名" value="${order.buyerName }">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>客户折扣率<em class="red">(如需调整,请在客户管理处调整)</em></label>
               	<div class="row" >
		            <div class="col-xs-10">
						<input id="customerDiscount" type="text" class="form-control" placeholder="请输入客户折扣率" readonly value="${order.customerDiscount }">
		            </div>
		            <div class="col-xs-2">
		            	%
		            </div>
	            </div>
            </div>
            <div class="form-group">
                <label>购买人微信</label>
                <input id="buyerWeixin" type="text" class="form-control"  placeholder="请输入购买人微信" value="${order.buyerWeixin }">
            </div>
            <div class="form-group">
                <label><em class="red">*</em>购买人地址</label>
                <input id="address" type="text" class="form-control"  placeholder="请输入购买人地址" value="${order.address }">
            </div>
            <div class="form-group">
                <label>订单状态</label>
                <select id="status" class="form-control">
                    <option value="1" <c:if test="${order.status == 1}">selected="selected"</c:if>>新建</option>
                    <option value="2" <c:if test="${order.status == 2}">selected="selected"</c:if>>备货中</option>
                    <option value="3" <c:if test="${order.status == 3}">selected="selected"</c:if>>已发货</option>
                    <option value="4" <c:if test="${order.status == 4}">selected="selected"</c:if>>已收货</option>
                    <option value="5" <c:if test="${order.status == 5}">selected="selected"</c:if>>完毕</option>
                    <option value="6" <c:if test="${order.status == 6}">selected="selected"</c:if>>退款退货</option>
                    <option value="7" <c:if test="${order.status == 7}">selected="selected"</c:if>>取消</option>
                </select>
            </div>
            <div class="form-group">
                <label>支付状态</label>
                <select id="payStatus" class="form-control">
                    <option value="0" <c:if test="${order.payStatus == 0}">selected="selected"</c:if>>未支付</option>
                    <option value="1" <c:if test="${order.payStatus == 1}">selected="selected"</c:if>>支付中</option>
                    <option value="2" <c:if test="${order.payStatus == 2}">selected="selected"</c:if>>已支付</option>
                </select>
            </div>
            <div class="form-group">
                <label>备注</label>
                <textarea id="remark" class="form-control" rows="5" placeholder="请填写订单备注信息">${order.remark }</textarea>
            </div>
            <div class="form-group">
                <label>发货日期</label>
                <input id="sendTime" type="text" class="form-control" readonly  placeholder="请输入发货日期" value="${order.sendTime }">
            </div>
            <div class="form-group">
                <label>快递公司</label>
                <input id="sendCompany" type="text" class="form-control"  placeholder="请输入快递公司" value="${order.sendCompany }">
            </div>
            <div class="form-group">
                <label>发货单号</label>
                <input id="sendNo" type="text" class="form-control"  placeholder="请输入发货单号" value="${order.sendNo }">
            </div>
        </div>
    </form>
    <div style="height: 100px"></div>
    <div class="new-brn-tab">
        <div class="new-tbl-type">
            <a id="caogaoBnt" class="new-tbl-cell btn btn-primary">存草稿</a>
            <a id="saveBnt" class="new-tbl-cell btn btn-submit">提交</a>
            <a href="<%=basePath%>/admin/order" class="new-tbl-cell btn btn-cancel">取消</a>
            <input id="buttonFlag" type="hidden" class="form-control">
        </div>
    </div>
</section>
</div>
</body>
</html>