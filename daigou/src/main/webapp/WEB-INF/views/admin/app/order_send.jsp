<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <link href="<%=basePath%>/static/admin/app/js/datapicker/datepicker3.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/datapicker/bootstrap-datepicker.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/datapicker/bootstrap-datepicker.zh-CN.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/fileUpload.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/order_send.js"></script>
    <title>订单管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/order/${order.id}/details" class="return"><i class="iconfont"></i>返回</a>
        <h2>订单发货</h2>
    </div>
</header>
<section>
    <form role="form">
        <input id="id" type="hidden" class="form-control"  readonly value="${order.id }">
        <input id="ts" type="hidden" class="form-control"  readonly value="${order.ts }">
        <div class="form-box">
        	<div class="form-group">
                <label>发货日期</label>
                <input id="sendTime" type="text" class="form-control" readonly placeholder="请输入发货日期" value="<fmt:formatDate value="${order.sendTime }" pattern="yyyy-MM-dd"/>">
            </div>
            <div class="form-group">
                <label>快递公司</label>
                <input id="sendCompany" type="text" class="form-control" placeholder="请输入快递公司" value="${order.sendCompany }">
            </div>
            <div class="form-group">
                <label>发货单号</label>
                <input id="sendNo" type="text" class="form-control" placeholder="请输入发货单号" value="${order.sendNo }">
            </div>
            <div class="form-group">
                <label>备注</label>
                <textarea id="remark" class="form-control" rows="5" placeholder="请填写订单备注信息">${order.remark }</textarea>
            </div>
        </div>
    </form>
    <div style="height: 100px"></div>
    <div class="new-brn-tab">
        <div class="new-tbl-type">
            <a id="saveSendBnt" class="new-tbl-pwd btn btn-submit">提交</a>
            <a href="<%=basePath%>/admin/order/${order.id}/details" class="new-tbl-pwd btn btn-cancel">取消</a>
        </div>
    </div>
</section>
</div>
</body>
</html>