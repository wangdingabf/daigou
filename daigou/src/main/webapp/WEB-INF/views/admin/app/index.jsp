<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/claim.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/footer.js"></script>
    <title>${systemTitle}</title>
</head>
<body>
<div class="viewport">
    <header>
        <div class="header-bg">
            	<a href="<%=basePath%>/${shop.code}" target="_blank"><font size="3">&nbsp;&nbsp;点我进入商城</font></a>
            <div class="federal-text">
                <div class="tu-logo" style="height:50px;">
                   <a href="<%=basePath%>/${shop.code}" target="_blank"><font size="5">欢迎您,${user.name }</font></a>
                </div>
                <div class="free-credit-box">
                    <a href="<%=basePath%>/admin/report" class="credit-limit">
                        <span>本月营业额</span>
                        <span>¥${orderSum.totalAmount}</span>
                    </a>
                    <em class="line-h"></em>
                    <a href="<%=basePath%>/admin/report" class="credit-limit">
                        <span>本月订单数</span>
                        <span>${totalCount}</span>
                    </a>
                </div>
                <div class="btn-box">
                    <a href="<%=basePath%>/admin/order/0" class="push-button">快速下单</a>
                </div>
            </div>
        </div>
    </header>
    <section>
	    <div class="news-box">
	        <div class="info-tip">
	             <span class="mark01"><i class="iconfont"></i>表示新订单消息提醒</span>
	             <span class="complete01"><i class="iconfont"></i>表示订单有新的支付消息提醒</span>
	        </div>
	        <ul class="news-box-list">
	        	<c:if test="${empty orderList}"><span>您暂时还没有新订单,加油哦！！！</span></c:if>
		        <c:forEach var="order" items="${orderList}">
		        <!-- li标签 -->
		        <c:if test="${order.status == 1}"><li class="news-tab mark"></c:if>
		        <c:if test="${order.status != 1}">
		        <c:if test="${not empty order.payStatus}"><li class="news-tab complete"></c:if>
		        <c:if test="${empty order.payStatus}"><li class="news-tab"></c:if>
				</c:if>
                    <i class="iconfont"></i>
                    <div class="img news-tab-item">
                        <img src="${order.picMain }" style="width:100px;height:90px;">
                    </div>
                    <div class="news-tab-item news-tab-caption">
                    	<!-- a标签 -->
                		<a href="<%=basePath%>/admin/order/${order.id }/details">
                        <div class="news-tab-title">
                            <span>单号:${order.id }</span>
                            <span class="news-tab-time">${order.statusName }</span>
                        </div>
                        <div class="news-tab-number">
                            <span>${order.productName }</span>
                            <span>${order.buyerName }</span>
                            <div class="type-Claim">
                                 <span>￥${order.price }(x${order.num })</span>
                                 <em class="news-tab-price">¥${order.totalAmount }</em>
                            </div>
                        </div>
                        </a>
                    </div>
	            </li>
		        </c:forEach>
	        </ul>
	    </div>
	</section>
</div>
<div style="height: 60px"></div>
<footer>
    <%@ include file="footer.jsp" %>
</footer>
<script type="text/javascript">
//5分钟刷新一次5*60000
setTimeout(goToRefreshOrders, 5*60000);
//跳转list页
function goToRefreshOrders(){
	window.location.href='<%=basePath%>/admin/index';
}
</script>
</body>
</html>