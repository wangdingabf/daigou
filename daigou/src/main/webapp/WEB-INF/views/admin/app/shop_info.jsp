<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/basic.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <title>店铺管理</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
        <a href="<%=basePath%>/admin/account" class="return"><i class="iconfont"></i>返回</a>
        <h2>店铺信息</h2>
        <!-- 只有店铺管理员才能编辑 -->
        <c:if test="${user.type == 1 }">
        <a href="<%=basePath%>/admin/shop-update" class="news-a-brn">编辑</a>
        </c:if>
    </div>
</header>
<article>
    <div class="info-con01">
        <h2 class="info-title">店铺主要信息</h2>
        <ul>
            <li>
                <span>店码：</span>
                <span>${shop.code }</span>
            </li>
            <li>
                <span>名称：</span>
                <span>${shop.name }</span>
            </li>
            <li>
                <span>logo：</span>
                <span><img id="logoImage" src="${shop.logo }" width="98" height="100"/></span>
            </li>
            <li>
                <span>描述：</span>
                <span>${shop.description }</span>
            </li>
        </ul>
    </div>
    <div class="info-con01">
        <h2 class="info-title">店铺收钱码</h2>
        <ul>
        	<li>
                <span>微信收钱码：</span>
                <span><img id="weixinQrCodeImage" src="${shop.weixinQrCode }" width="98" height="100"/></span>
            </li>
            <li>
                <span>支付宝收钱码：</span>
                <span><img id="zhifubaoQrCodeImage" src="${shop.zhifubaoQrCode }" width="98" height="100"/></span>
            </li>
        </ul>
    </div>
    <div class="info-con01">
        <h2 class="info-title">店铺地址</h2>
        <ul>
        	<li>
                <span>所在省/市：</span>
                <span>${shop.province }</span>
            </li>
            <li>
                <span>所在市/区：</span>
                <span>${shop.city }</span>
            </li>
            <li>
                <span>详细地址：</span>
                <span>${shop.address }</span>
            </li>
        </ul>
    </div>
    <div class="info-con01">
        <h2 class="info-title">店铺状态</h2>
        <ul>
        	<li>
                <span>状态：</span>
                <span>
                	<c:if test="${shop.status == 1 }">正常</c:if>
                	<c:if test="${shop.status == 0 }">冻结</c:if>
                </span>
            </li>
            <li>
                <span>有效期：</span>
                <span>
                	<fmt:formatDate value="${shop.startTime }" pattern="yyyy-MM-dd"/>
                	至
                	<fmt:formatDate value="${shop.endTime }" pattern="yyyy-MM-dd"/>
               	</span>
            </li>
        </ul>
    </div>
</article>
</div>
</body>
</html>
