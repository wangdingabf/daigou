function show_pic_view_all(widthSize,heightSize,imgId){
	 //设置正方形容器的位置为相对定位，溢出内容隐藏
    $("."+imgId+"contain").css("position","relative").css("overflow","hidden");
    $("."+imgId+"contain").width(widthSize).height(heightSize);//设置容器的大小
    $("img[id^="+imgId+"]").each(function() {
		var width = $(this).width();
		var height = $(this).height();
		console.log(width+"gao:"+height);
		var _size = width;
		if(width>height){
			_size = height;//得到最小的边长
		}
		var divSize = widthSize;
		if(divSize < heightSize){
			divSize = heightSize;//得到最小的边长
		}
		//按比例将得到图片缩小后的长宽
		var widthScaling = _size/divSize;
		var heightScaling = _size/divSize;
		height = height/(heightScaling);
		width = width/(widthScaling);
		var top = (height - heightSize)/-2;
		//得到图片在正方形容器中的偏移位置
		var left = (width - widthSize)/-2;
		$(this).width(width).height(height);//设置图片的大小
		$(this).css("position","absolute");//设置图片为绝对定位
		//设置图片相对容器的位置
		$(this).css("top",top+"px").css("left",left+"px");
    });
}
function show_pic_view(widthSize,heightSize,imgId){
	 //设置正方形容器的位置为相对定位，溢出内容隐藏
	$("."+imgId+"contain").css("position","relative").css("overflow","hidden");
    $("."+imgId+"contain").width(widthSize).height(heightSize);//设置容器的大小
	var width = $("#"+imgId).width();
	var height = $("#"+imgId).height();
	var _size = width;
	if(width>height){
		_size = height;//得到最小的边长
	}
	var divSize = widthSize;
	if(divSize < heightSize){
		divSize = heightSize;//得到最小的边长
	}
	//按比例将得到图片缩小后的长宽
	var widthScaling = _size/divSize;
	var heightScaling = _size/divSize;
	height = height/(heightScaling);
	width = width/(widthScaling);
	var top = (height - heightSize)/-2;
	//得到图片在正方形容器中的偏移位置
	var left = (width - widthSize)/-2;
	$("#"+imgId).width(width).height(height);//设置图片的大小
	$("#"+imgId).css("position","absolute");//设置图片为绝对定位
	//设置图片相对容器的位置
	$("#"+imgId).css("top",top+"px").css("left",left+"px");
}