//表单验证
$(function(){
	function listProduct(){
		//获取form的值
		var searchKey = $('#searchInput').val();
		var productQuery = {};
		productQuery["searchKey"] = searchKey;
		var params = JSON.stringify(productQuery);
		$.ajax({
			url:baselocation+'/product-query',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: params,
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					var totalCount = result.data.totalCount;
					var onSaleCount = result.data.onSaleCount;
					var offSaleCount = result.data.offSaleCount;
					var caogaoCount = result.data.caogaoCount;
					$("#totalCountLi").html("商品总数："+totalCount);
					$("#onSaleCountLi").html("上架:"+onSaleCount);
					$("#offSaleCountLi").html("下架:"+offSaleCount);
					$("#caogaoCountLi").html("草稿:"+caogaoCount);
					
					var productList = result.data.productList;
					var jsonStr = "";
					if(productList.length > 0){
						for(var i=0;i<productList.length;i++){
							var product = productList[i];
							jsonStr += "<a href='"+baselocation+"/product/"+product.id+"/details' class='details-link'>";
							jsonStr += "<ul class='details-list-image'>";
							jsonStr += "<li>";
							jsonStr += "<img id='picMainImage' src='"+product.picMain+"' width='73.5' height='75'/>";
							jsonStr += "</li>";
							jsonStr += "<li>"+product.name+"</br>"+product.spec+"</li>";
							jsonStr += "<li><s>￥"+product.marketPrice+"</s></br>￥"+product.salePrice+"</li>";
							jsonStr += "<li>"+product.statusName+"</li>";
							jsonStr += "</ul>";
							jsonStr += "</a>";
						}
					}
					
					$("#detailBoxDiv").html(jsonStr);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#searchBtn').click(function () {
		listProduct();
	})
	$('#searchInput').blur(function () {
		listProduct();
	})
});