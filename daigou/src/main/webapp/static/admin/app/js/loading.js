//动态分页加载
function loaded () {
    var myScroll,
        upIcon = $("#up-icon"),
        downIcon = $("#down-icon"),
        distance = 30; //滑动距离
  //重设高度
    var detailBoxDivHeight = $('#detailBoxDiv').height();
    //var browser_width = $(document.body).height();
    if(detailBoxDivHeight <= 240){
    	$("#detailBoxDiv").css("minHeight","240px");
    }
    myScroll = new IScroll('#wrapper', { probeType: 3, mouseWheel: true, click:true });
    	
    myScroll.on("scroll",function(){
        var y = this.y,
            maxY = this.maxScrollY - y,
            downHasClass = downIcon.hasClass("reverse_icon"),
            upHasClass = upIcon.hasClass("reverse_icon");
        console.log(this.maxScrollY);
    	console.log(this.y);
    	console.log(maxY);
        if(y >= distance){
            !downHasClass && downIcon.addClass("reverse_icon");
            return "";
        }else if(y < distance && y > 0){
            downHasClass && downIcon.removeClass("reverse_icon");
            return "";
        }

        if(maxY >= distance){
            !upHasClass && upIcon.addClass("reverse_icon");
            return "";
        }else if(maxY < distance && maxY >=0){
            upHasClass && upIcon.removeClass("reverse_icon");
            return "";
        }
    });
    var pageIndex=1;
    var pageSize=20;
    function upAjax(){
    	//获取form的值
		var searchKey = $('#searchInput').val();
		var statusInput = $('#statusInput').val();
		var payInput = $('#payInput').val();
		var timeInput = $('#timeInput').val();
		var orderQuery = {};
		orderQuery["searchKey"] = searchKey;
		orderQuery["status"] = statusInput;
		orderQuery["timeType"] = timeInput;
		orderQuery["payStatus"] = payInput;
		pageIndex++;
		orderQuery["pageIndex"] = pageIndex;
		orderQuery["pageSize"] = pageSize;
        var params = JSON.stringify(orderQuery);
        $.ajax({
            type: "post",
            url: baselocation+'/order-query',
            data: params,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(function(result) {
            if (result && result.success && result.data.orderList.length > 0) {
            	var totalCount = result.data.totalCount;
				$("#totalCountLi").html("订单总数："+totalCount);
				var caogaoCount = result.data.caogaoCount;
				$("#totalCaogaoLi").html("草稿："+caogaoCount);
				
				var totalAmount = result.data.totalAmount;
				$("#totalAmountSpan").html("￥"+totalAmount);
				
				var payAmount = result.data.payAmount;
				$("#payAmountSpan").html("￥"+payAmount);
				
				var noPayAmount = result.data.noPayAmount;
				$("#noPayAmountSpan").html("￥"+noPayAmount);
				
				var orderList = result.data.orderList;
				var jsonStr = "";
				if(orderList.length > 0){
					for(var i=0;i<orderList.length;i++){
						var order = orderList[i];
						jsonStr += "<a href='"+baselocation+"/order/"+order.id+"/details' class='details-link'>";
						jsonStr += "<ul class='details-list-image'>";
						jsonStr += "<li style='width:30%;'>";
						jsonStr += "<div class='new-ad-img new-p-re'>";
						jsonStr += "<img id='picMainImage' style='margin: 0 auto;' src='"+order.picMain +"' width='120' height='75'/>";
						jsonStr += "<span>"+order.id+"</span>";
						jsonStr += "</div>";
						jsonStr += "</li>";
						jsonStr += "<li style='width:25%;'>"+order.productName+"</br>"+order.spec+"</li>";
						jsonStr += "<li style='width:25%;'>￥"+order.price.toFixed(2)+"  X  "+order.num.toFixed(2)+"</br><font color='red'>总：</font>￥"+order.totalAmount.toFixed(2)+"</li>";
						jsonStr += "<li style='width:20%;'><font color='red'>"+order.statusName+"</font></br>"+order.buyerName+"</li>";
						jsonStr += "</ul>";
						jsonStr += "</a>";
					}
				}
				
                $('#detailBoxDiv').append(jsonStr);
                myScroll.refresh(totalCount);
            }else{
            	$("#pullUp-msg").html("别拉了,到底了,没有更多数据了！！！");
            }
        }).fail(function() {
        	$("#detailBoxDiv").html("<font style='font-size:0.16rem;'>数据请求失败，请重新刷新</font>");
        })
    }

    function downAjax(){
    	//获取form的值
		var searchKey = $('#searchInput').val();
		var statusInput = $('#statusInput').val();
		var payInput = $('#payInput').val();
		var timeInput = $('#timeInput').val();
		var orderQuery = {};
		orderQuery["searchKey"] = searchKey;
		orderQuery["status"] = statusInput;
		orderQuery["timeType"] = timeInput;
		orderQuery["payStatus"] = payInput;
		pageIndex = 1;
		orderQuery["pageIndex"] = pageIndex;
		orderQuery["pageSize"] = pageSize;
        var params = JSON.stringify(orderQuery);
        $.ajax({
            type: "post",
            url: baselocation+'/order-query',
            data: params,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(function(result) {
        	if (result && result.success && result.data.orderList.length > 0) {
            	var totalCount = result.data.totalCount;
				$("#totalCountLi").html("订单总数："+totalCount);
				var caogaoCount = result.data.caogaoCount;
				$("#totalCaogaoLi").html("草稿："+caogaoCount);
				
				var totalAmount = result.data.totalAmount;
				$("#totalAmountSpan").html("￥"+totalAmount);
				
				var payAmount = result.data.payAmount;
				$("#payAmountSpan").html("￥"+payAmount);
				
				var noPayAmount = result.data.noPayAmount;
				$("#noPayAmountSpan").html("￥"+noPayAmount);
				
				var orderList = result.data.orderList;
				var jsonStr = "";
				if(orderList.length > 0){
					for(var i=0;i<orderList.length;i++){
						var order = orderList[i];
						jsonStr += "<a href='"+baselocation+"/order/"+order.id+"/details' class='details-link'>";
						jsonStr += "<ul class='details-list-image'>";
						jsonStr += "<li style='width:30%;'>";
						jsonStr += "<div class='new-ad-img new-p-re'>";
						jsonStr += "<img id='picMainImage' style='margin: 0 auto;' src='"+order.picMain +"' width='120' height='75'/>";
						jsonStr += "<span>"+order.id+"</span>";
						jsonStr += "</div>";
						jsonStr += "</li>";
						jsonStr += "<li style='width:25%;'>"+order.productName+"</br>"+order.spec+"</li>";
						jsonStr += "<li style='width:25%;'>￥"+order.price.toFixed(2)+"  X  "+order.num.toFixed(2)+"</br><font color='red'>总：</font>￥"+order.totalAmount.toFixed(2)+"</li>";
						jsonStr += "<li style='width:20%;'><font color='red'>"+order.statusName+"</font></br>"+order.buyerName+"</li>";
						jsonStr += "</ul>";
						jsonStr += "</a>";
					}
				}
				
                $('#detailBoxDiv').html(jsonStr);
                myScroll.refresh(totalCount);
            }
        }).fail(function() {
        	$("#detailBoxDiv").html("<font style='font-size:0.16rem;'>数据请求失败，请重新刷新</font>");
        })
    }
    // 下拉刷新事件
    myScroll.on("slideDown",function(){
        if(this.y > distance){
        	$("#pullUp-msg").html("上拉加载更多");
            downAjax();
            upIcon.removeClass("reverse_icon")
        }
    });
    // 上拉滑动事件
    myScroll.on("slideUp",function(){
    	console.log(this.maxScrollY);
    	console.log(this.y);
        if(this.maxScrollY - this.y > distance){
            upAjax();
            upIcon.removeClass("reverse_icon")
        }
    });
}
loaded ();