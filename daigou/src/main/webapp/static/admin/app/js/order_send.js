//表单验证
$(function(){
	$("#sendTime").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language:"zh-CN",
        format: "yyyy-mm-dd"
    });
	//
	function updateOrderStatus(status){
		//获取form的值
		var id = $('#id').val();
		var ts = $('#ts').val();
		
		var order = {};
		order["id"] = id;
		order["ts"] = ts;
		order["status"] = status;
		if(status == 3){
			var sendCompany = $('#sendCompany').val();
			var sendNo = $('#sendNo').val();
			var sendTime = $('#sendTime').val();
			order["sendCompany"] = sendCompany;
			order["sendNo"] = sendNo;
			order["sendTime"] = sendTime;
			if(sendCompany != null && sendCompany.length > 32){
				layer.msg("快递公司不能超过32位!",{icon:2,time:2000});
				return;
			}
			if(sendNo != null && sendNo.length > 32){
				layer.msg("快递单号不能超过32位!",{icon:2,time:2000});
				return;
			}
		}
		
		$.ajax({
			url:baselocation+'/order-update-status',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: JSON.stringify(order),
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					layer.msg("订单发货成功!",{icon:1,time:2000});
					setTimeout(goToOrderDetails, 2000);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#saveSendBnt').click(function () {
		updateOrderStatus('3');
	})
	//跳转list页
	function goToOrderDetails(){
		var id = $('#id').val();
		window.location.href=baselocation+'/order/'+id+'/details';
	}
});