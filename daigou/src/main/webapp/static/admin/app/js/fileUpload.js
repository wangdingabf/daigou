function ajaxFileUpload(uploadUrl,fileId,imageId){
	var formData = new FormData();
	var file = $("#"+fileId).get(0).files[0];
	/* 压缩图片 */
    lrz(file, {
        quality:0.4
    }).then(function (rst) {
        /* 处理成功后执行rst.formData, */
    	$.ajax({ 
    		url : baselocation+uploadUrl,
    		type : 'POST',
    		data : rst.formData,
    		cache: false,
    		// 告诉jQuery不要去处理发送的数据
    		processData : false,
    		// 告诉jQuery不要去设置Content-Type请求头
    		contentType : false,
    		dataType:'json',
    		beforeSend:function(){
    			console.log("正在进行，请稍候");
    		},
    		success:function(result){
    			if(result.success==false){
    				layer.msg(result.message,{icon:2,time:1500});
    			}else{
    				//显示图片
    				//showImage(imageId, imagelocation+result.data);
    				$("#"+imageId).attr("src",imagelocation+result.data);
    			}
    		},
    		error:function(result){
    			alert("系统异常,请联系管理员！");
    		}
    	});
    }).catch(function (err) {
        /* 处理失败后执行 */
    	alert("系统异常,请联系管理员！");
    }).always(function () {
        /* 必然执行 */
    })
	
}

function ajaxFileUpload_bak(uploadUrl,fileId,imageId){
	var formData = new FormData();
	var file = $("#"+fileId).get(0).files[0];
	formData.append("file",file);
	$.ajax({ 
		url : baselocation+uploadUrl,
		type : 'POST',
		data : formData,
		cache: false,
		// 告诉jQuery不要去处理发送的数据
		processData : false,
		// 告诉jQuery不要去设置Content-Type请求头
		contentType : false,
		dataType:'json',
		beforeSend:function(){
			console.log("正在进行，请稍候");
		},
		success:function(result){
			if(result.success==false){
				layer.msg(result.message,{icon:2,time:1500});
			}else{
				//显示图片
				//showImage(imageId, imagelocation+result.data);
				$("#"+imageId).attr("src",imagelocation+result.data);
			}
		},
		error:function(result){
			alert("系统异常,请联系管理员！");
		}
	});
}