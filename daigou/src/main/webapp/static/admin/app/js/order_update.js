//表单验证
$(function(){
	$("#sendTime").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language:"zh-CN",
        format: "yyyy-mm-dd"
    });
	$("#price").TouchSpin({
        min: 0,
        max: 10000000,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
	$("#num").TouchSpin({
        min: 1,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
	function updateOrder(type){
		//判断冻结标记"frezz"
		var buttonFlag = $('#buttonFlag').val();
		var nowFlag = new Date().valueOf();
		if(buttonFlag != null && buttonFlag != ""){
			if(nowFlag - buttonFlag < 2000){
				return;
			}
		}
		//设置冻结
		$('#buttonFlag').val(nowFlag);
		//获取form的值
		var id = $('#id').val();
		var ts = $('#ts').val();
		var productId = $('#productId').val();
		var productName = $('#productName').val();
		var price = $('#price').val();
		var num = $('#num').val();
		
		var spec = $('#spec').val();
		var cate = $('#cate').val();
		var brand = $('#brand').val();
		var productionPlace = $('#productionPlace').val();
		
		
		var buyerCode = $('#buyerCode').val();
		var buyerName = $('#buyerName').val();
		var buyerWeixin = $('#buyerWeixin').val();
		var address = $('#address').val();
		
		var status = $('#status').val();
		var payStatus = $('#payStatus').val();
		var remark = $('#remark').val();
		var picMainImage = $('#picMainImage').attr("src");
		
		var sendCompany = $('#sendCompany').val();
		var sendNo = $('#sendNo').val();
		var sendTime = $('#sendTime').val();
		//是不是草稿，下单也必须有商品
		if(productName == null || productName == ""){
			layer.msg("请选择一个商品或自定义商品名称!",{icon:2,time:2000});
			return;
		}
		//如果不是草稿，需以下校验
		if(type != 0){
			if(price == null || price == ""){
				layer.msg("请输入商品价格!",{icon:2,time:2000});
				return;
			}
			if(num == null || num == ""){
				layer.msg("请输入购买数量!",{icon:2,time:2000});
				return;
			}
			
			if(buyerCode == null || buyerCode == ""){
				layer.msg("请输入购买人联系电话!",{icon:2,time:2000});
				return;
			}
			if(!isPhoneNo(buyerCode)){
				layer.msg("请输入11位正常手机号!",{icon:2,time:2000});
				return;
			}
			if(buyerName == null || buyerName == ""){
				layer.msg("请输入购买人姓名!",{icon:2,time:2000});
				return;
			}
			
			if(address == null || address == ""){
				layer.msg("请输入购买人收货地址!",{icon:2,time:2000});
				return;
			}
			
			if(picMainImage == null || picMainImage == ""){
				layer.msg("请上传商品图片,方便向客户展示!",{icon:2,time:2000});
				return;
			}
		}
		if(buyerWeixin != null && buyerWeixin.length > 32){
			layer.msg("微信号不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(productName != null && productName.length > 32){
			layer.msg("商品名称不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(picMainImage != null && picMainImage.length > 512){
			layer.msg("商品图片url不能超过512位!",{icon:2,time:2000});
			return;
		}
		if(remark != null && remark.length > 512){
			layer.msg("备注不能超过512位!",{icon:2,time:2000});
			return;
		}
		if(spec != null && spec.length > 32){
			layer.msg("规格不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(cate != null && cate.length > 32){
			layer.msg("类别不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(brand != null && brand.length > 32){
			layer.msg("品牌不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(productionPlace != null && productionPlace.length > 100){
			layer.msg("产地不能超过100位!",{icon:2,time:2000});
			return;
		}
		if(sendCompany != null && sendCompany.length > 32){
			layer.msg("快递公司不能超过32位!",{icon:2,time:2000});
			return;
		}
		if(sendNo != null && sendNo.length > 32){
			layer.msg("快递单号不能超过32位!",{icon:2,time:2000});
			return;
		}
		
		var order = {};
		order["id"] = id;
		order["ts"] = ts;
		order["productId"] = productId;
		order["productName"] = productName;
		order["price"] = price;
		order["num"] = num;
		
		//0是草稿状态
		if(type == 0){
			//-1：删除，0，草稿，1，正常
			order["dr"] = 0;
		}
		order["status"] = status;
		order["payStatus"] = payStatus;
		
		order["picMain"] = picMainImage;
		order["remark"] = remark;
		
		order["buyerCode"] = buyerCode;
		order["buyerName"] = buyerName;
		order["buyerWeixin"] = buyerWeixin;
		order["address"] = address;
		
		order["spec"] = spec;
		order["cate"] = cate;
		order["brand"] = brand;
		order["productionPlace"] = productionPlace;
		
		order["sendCompany"] = sendCompany;
		order["sendNo"] = sendNo;
		order["sendTime"] = sendTime;
		
//		$.ajax({
//			url:baselocation+'/order',
//			contentType:'application/json',
//			type:'post',
//			dataType:'json',
//			data: JSON.stringify(order),
//			success:function(result){
//				if(result.success==false){
//					layer.msg(result.message,{icon:2,time:2000});
//				}else{
//					window.location.href=baselocation+'/order/'+result.data+'/details';
//				}
//			},
//			error:function(result){
//				alert("系统异常,请联系管理员！");
//			}
//		});
		//总金额
		var confirmMessage = "";
		if(type == 0){
			confirmMessage = "确认提交草稿？";
		}else{
			var orderAmount = price * num;
			var customerDiscount = $("#customerDiscount").val();
			if(customerDiscount == null || customerDiscount == ''){
				customerDiscount = 100;
			}
			var totalAmount = orderAmount * customerDiscount / 100;
			confirmMessage = "订单金额：￥"+ orderAmount + "</br>"
			confirmMessage += "客户折扣率：" + customerDiscount + "%</br>";
			confirmMessage += "折后总额：￥" + totalAmount.toFixed(2) + "</br>";
			confirmMessage += "确认提交？";
			
			order["customerDiscount"] = customerDiscount;
			order["orderAmount"] = orderAmount;
			
			order["totalAmount"] = totalAmount;
		}
		layer.confirm(confirmMessage, {
			btn: ['确认','取消'], //按钮
			icon: 3, 
			title:'操作提示',
			skin: 'layui-layer-demo', //样式类名
			closeBtn: false, //不显示关闭按钮
			shadeClose: true, //开启遮罩关闭
			shade: false //不显示遮罩
		}, function(){
			//判断冻结标记"frezz"
			var buttonFlag = $('#buttonFlag').val();
			var nowFlag = new Date().valueOf();
			if(buttonFlag != null && buttonFlag != ""){
				if(nowFlag - buttonFlag < 2000){
					return;
				}
			}
			//设置冻结
			$('#buttonFlag').val(nowFlag);
			$.ajax({
				url:baselocation+'/order',
				contentType:'application/json',
				type:'post',
				dataType:'json',
				data: JSON.stringify(order),
				success:function(result){
					if(result.success==false){
						layer.msg(result.message,{icon:2,time:2000});
					}else{
						window.location.href=baselocation+'/order/'+result.data+'/details';
					}
				},
				error:function(result){
					alert("系统异常,请联系管理员！");
				}
			});
		}, function(){
			//取消
		});
	}
	
	$('#saveBnt').click(function () {
		updateOrder(1);
	})
	
	$('#caogaoBnt').click(function () {
		updateOrder(0);
	})
	$('#doMyMindBtn').click(function () {
		var jsonStr = "<label><em class='red'>*</em>商品名称</label>";
		jsonStr += "<input id='productName' type='text' class='form-control' placeholder='请输入商品名称'>";
        
		$("#productNameDiv").html(jsonStr);
	})
	
	$('#buyerCode').blur(function () {
		var buyerCode = $('#buyerCode').val();
		if(buyerCode == null || buyerCode == ''){
			layer.msg("请输入您的手机号!",{icon:2,time:2000});
			return;
		}
		$.ajax({
			url:baselocation+'/customer-query/'+$('#buyerCode').val(),
			type:'get',
			dataType:'json',
			success:function(result){
				if(result.success==true){
					$("#buyerName").val(result.data.customerName);
					$("#address").val(result.data.address);
					$("#customerDiscount").val(result.data.customerDiscount);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	})
});