//表单验证
$(function(){
	//
	function updateOrderStatus(status){
		//获取form的值
		var id = $('#id').val();
		var ts = $('#ts').val();
		
		var order = {};
		order["id"] = id;
		order["ts"] = ts;
		order["status"] = status;
		if(status == 3){
			var sendCompany = $('#sendCompany').val();
			var sendNo = $('#sendNo').val();
			var sendTime = $('#sendTime').val();
			order["sendCompany"] = sendCompany;
			order["sendNo"] = sendNo;
			order["sendTime"] = sendTime;
			if(sendCompany != null && sendCompany.length > 32){
				layer.msg("快递公司不能超过32位!",{icon:2,time:2000});
				return;
			}
			if(sendNo != null && sendNo.length > 32){
				layer.msg("快递单号不能超过32位!",{icon:2,time:2000});
				return;
			}
		}
		
		$.ajax({
			url:baselocation+'/order-update-status',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: JSON.stringify(order),
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					layer.msg(result.message,{icon:1,time:2000});
					setTimeout(goToOrderDetails, 2000);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#confirmBnt').click(function () {
		updateOrderStatus('2');
	})
	$('#sendBnt').click(function () {
		var id = $('#id').val();
		window.location.href=baselocation+'/order/'+id+'/send';
	})
	$('#receiveBnt').click(function () {
		updateOrderStatus('4');
	})
	$('#overBnt').click(function () {
		updateOrderStatus('5');
	})
	$('#refoundBnt').click(function () {
		layer.confirm('是否确认退款退货？', {
		    btn: ['确认','取消'], //按钮
		    icon: 3, 
		    title:'操作提示',
		    skin: 'layui-layer-demo', //样式类名
		    closeBtn: false, //不显示关闭按钮
		    shadeClose: true, //开启遮罩关闭
		    shade: false //不显示遮罩
		}, function(){
			updateOrderStatus('6');
		}, function(){
		    //取消
		});
	})
	$('#cancelBnt').click(function () {
		layer.confirm('是否确认取消订单？', {
		    btn: ['确认','取消'], //按钮
		    icon: 3, 
		    title:'操作提示',
		    skin: 'layui-layer-demo', //样式类名
		    closeBtn: false, //不显示关闭按钮
		    shadeClose: true, //开启遮罩关闭
		    shade: false //不显示遮罩
		}, function(){
			updateOrderStatus('7');
		}, function(){
		    //取消
		});
	})
	
	$('#deleteBnt').click(function () {
		layer.confirm('订单删除后不可恢复,是否确认？', {
		    btn: ['确认','取消'], //按钮
		    icon: 3, 
		    title:'操作提示',
		    skin: 'layui-layer-demo', //样式类名
		    closeBtn: false, //不显示关闭按钮
		    shadeClose: true, //开启遮罩关闭
		    shade: false //不显示遮罩
		}, function(){
			var id = $('#id').val();
			$.ajax({
				url:baselocation+'/order/'+id,
				//contentType:'application/json',
				type:'delete',
				dataType:'json',
				//data: JSON.stringify(product),
				success:function(result){
					if(result.success==false){
						layer.msg(result.message,{icon:2,time:2000});
					}else{
						layer.msg(result.message,{icon:1,time:2000});
						setTimeout(goToOrderList, 2000);
					}
				},
				error:function(result){
					alert("系统异常,请联系管理员！");
				}
			});
		}, function(){
		    //取消
		});
	})
	//跳转list页
	function goToOrderList(){
		window.location.href=baselocation+'/order';
	}
	//跳转list页
	function goToOrderDetails(){
		var id = $('#id').val();
		window.location.href=baselocation+'/order/'+id+'/details';
	}
});