package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.ProductDto;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Product;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.ProductService;

@Controller("adminProductController")
@RequestMapping(value = "/admin")
public class ProductController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
	@Autowired
	private ProductService productService;
	
	/**
	 * 查看详情和编辑页
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/product/{id}",method = RequestMethod.GET)
    public String toProductPage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		Product product = null;
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		if(id == null || id.equals(0L)){
			LOGGER.info("进入商品新增{}",id);
			product = new Product();
			product.setId(id);
			product.setDr(0);
			model.addAttribute("productTitle", "商品新增");
		}else{
			LOGGER.info("进入商品更新{}",id);
			product = productService.selectByPrimaryKey(id);
			model.addAttribute("productTitle", "商品编辑");
		}
		model.addAttribute("product", product);
        return "/admin/app/product_update";
    }
	@RequestMapping(value = "/product/{id}/details",method = RequestMethod.GET)
    public String toProductDetailsPage(Model model,@PathVariable("id")Long id){
		LOGGER.info("进入商品详情{}",id);
		Product product = productService.selectByPrimaryKey(id);
		model.addAttribute("product", product);
        return "/admin/app/product_details";
    }
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/product",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult product(HttpServletRequest request,@RequestBody Product product){
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		product.setSalerId(shop.getSalerId());
		product.setShopId(shop.getId());
		product.setCreatorId(shop.getSalerId());
		product.setCreatedTime(new Date());
		ServiceResult serviceResult = productService.insertOrUpdate(product);
        return new AjaxResult(serviceResult);
    }
	@RequestMapping(value = "/product",method = RequestMethod.GET)
    public String toProductListPage(HttpServletRequest request,Model model){
		LOGGER.info("进入商品页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Product> productList = productService.selectByShopId(shop.getId(),null);
		model.addAttribute("productList", productList);
		Integer onSaleCount = productService.selectOnSaleCountByShopId(shop.getId());
		model.addAttribute("onSaleCount", onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByShopId(shop.getId());
		model.addAttribute("offSaleCount", offSaleCount);
		Integer totalCount = productService.selectTotalCountByShopId(shop.getId());
		model.addAttribute("totalCount", totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByShopId(shop.getId());
		model.addAttribute("caogaoCount", caogaoCount);
        return "/admin/app/product_list";
    }
	
	@RequestMapping(value = "/product-caogao",method = RequestMethod.GET)
    public String toProductCaogaoListPage(HttpServletRequest request,Model model){
		LOGGER.info("进入商品页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Product> productList = productService.selectCaogaoByShopId(shop.getId(),null);
		model.addAttribute("productList", productList);
        return "/admin/app/product_caogao";
    }
	
	@RequestMapping(value = "/order-product-list",method = RequestMethod.GET)
    public String toOrderProductListPage(HttpServletRequest request,Model model){
		LOGGER.info("从订单管理进入商品页面");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Product> productList = productService.selectByShopId(shop.getId(),null);
		model.addAttribute("productList", productList);
		Integer onSaleCount = productService.selectOnSaleCountByShopId(shop.getId());
		model.addAttribute("onSaleCount", onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByShopId(shop.getId());
		model.addAttribute("offSaleCount", offSaleCount);
		Integer totalCount = productService.selectTotalCountByShopId(shop.getId());
		model.addAttribute("totalCount", totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByShopId(shop.getId());
		model.addAttribute("caogaoCount", caogaoCount);
        return "/admin/app/order_product_list";
    }
	
	@RequestMapping(value = "/product-query",method = RequestMethod.GET)
	@ResponseBody
    public AjaxResult queryProductList(HttpServletRequest request,@RequestParam("searchKey")String searchKey){
		LOGGER.info("查询商品");
		Shop shop = (Shop)request.getAttribute(CommonConstants.SHOP_INFO);
		List<Product> productList = productService.selectByShopId(shop.getId(),searchKey);
		ProductDto productDto = new ProductDto();
		productDto.setProductList(productList);
		Integer onSaleCount = productService.selectOnSaleCountByShopId(shop.getId());
		productDto.setOnSaleCount(onSaleCount);
		Integer offSaleCount = productService.selectOffSaleCountByShopId(shop.getId());
		productDto.setOffSaleCount(offSaleCount);
		Integer totalCount = productService.selectTotalCountByShopId(shop.getId());
		productDto.setTotalCount(totalCount);
		Integer caogaoCount = productService.selectCaogaoCountByShopId(shop.getId());
		productDto.setCaogaoCount(caogaoCount);
		return new AjaxResult(true,productDto);
    }
}
