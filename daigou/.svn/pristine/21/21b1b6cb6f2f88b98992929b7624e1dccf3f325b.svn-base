package com.daigou.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.dto.AjaxResult;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.Pay;
import com.daigou.model.Shop;
import com.daigou.service.OrderService;
import com.daigou.service.PayService;
import com.daigou.service.ShopService;

@Controller("payController")
public class PayController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PayController.class);

	@Autowired
	private PayService payService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private ShopService shopService;
	
	@RequestMapping(value = "/order/{orderId}/pay",method = RequestMethod.GET)
    public String toPayPage(HttpServletRequest request,Model model,
    		@PathVariable("orderId")Long orderId){
		LOGGER.info("进入支付记录页面");
		Order order = orderService.selectByPrimaryKey(orderId);
		List<Pay> payList = payService.selectByOrderId(order.getShopId(), orderId);
		model.addAttribute("order", order);
		model.addAttribute("payList", payList);
		return "/shop/app/pay";
    }
	
	@RequestMapping(value = "/order/{orderId}/pay",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult pay(HttpServletRequest request,Model model,@RequestBody Pay pay,
    		@PathVariable("orderId")Long orderId){
		LOGGER.info("开始支付");
		Order order = orderService.selectByPrimaryKey(orderId);
		if(order == null){
			return new AjaxResult(false,"订单不存在,请联系店主或直接联系管理员!");
		}
		pay.setSalerId(order.getSalerId());
		pay.setShopId(order.getShopId());
		pay.setCreatorId(order.getCreatorId());
		pay.setCreatedTime(new Date());
		//0,待支付,1,确认支付,2,支付成功
		pay.setStatus(0);
		try{
			ServiceResult serviceResult = payService.insertPayAndUpdateOrder4Front(pay);
			return new AjaxResult(serviceResult);
		}catch(Exception ex){
			return new AjaxResult(false,ex.getMessage());
		}
    }
	@RequestMapping(value = "/pay/pay-status-update",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult payStatusUpdate(HttpServletRequest request,Model model,@RequestBody Pay pay){
		LOGGER.info("开始更新支付状态");
		//0,待支付,1,确认支付,2,支付成功
		pay.setStatus(1);
		try{
			int result = payService.updateByPrimaryKeySelective(pay);
			if(result == 1){
				return new AjaxResult(true,"支付成功，等待买家确认金额!");
			}else{
				return new AjaxResult(false,"支付失败,请重新支付或者联系卖家!");
			}
		}catch(Exception ex){
			return new AjaxResult(false,ex.getMessage());
		}
    }
	
	@RequestMapping(value = "/order/{orderId}/pay/{payId}/go-pay",method = RequestMethod.GET)
    public String toPayMethodPage(HttpServletRequest request,Model model,
    		@PathVariable("orderId")Long orderId,
    		@PathVariable("payId")Long payId){
		LOGGER.info("进入支付页面");
		Pay pay = payService.selectByPrimaryKey(payId);
		Order order = orderService.selectByPrimaryKey(orderId);
		Shop shop = shopService.selectByPrimaryKey(order.getShopId());
		model.addAttribute("shop", shop);
		String path = null;
		if("1".equals(pay.getPayMethod())){
			path = "/shop/app/ali_pay";
		}else if("2".equals(pay.getPayMethod())){
			path =  "/shop/app/wechat_pay";
		}else{
			path =  "/shop/app/other_pay";
		}
		model.addAttribute("order", order);
		model.addAttribute("pay", pay);
		return path;
    }
}
