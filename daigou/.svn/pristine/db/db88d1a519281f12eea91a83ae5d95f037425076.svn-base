/*
Navicat MySQL Data Transfer

Source Server         : localhost3308
Source Server Version : 50616
Source Host           : 127.0.0.1:3308
Source Database       : daigou_db

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2018-03-18 22:20:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` bigint(20) NOT NULL,
  `saler_id` bigint(20) NOT NULL,
  `shop_id` bigint(20) NOT NULL,
  `shop_name` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `num` decimal(16,2) NOT NULL,
  `cate` bigint(20) DEFAULT NULL,
  `spec` varchar(32) DEFAULT NULL,
  `brand` varchar(32) DEFAULT NULL,
  `buyer_code` varchar(32) NOT NULL,
  `buyer_name` varchar(32) NOT NULL,
  `address` varchar(512) NOT NULL,
  `pic_main` varchar(512) DEFAULT NULL,
  `remark` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0,暂存,1,待支付,2,待确认,3,待发货,4待收货,5,完成',
  `send_no` varchar(32) DEFAULT NULL,
  `send_company` varchar(32) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `pay_amount` decimal(16,2) DEFAULT NULL,
  `no_pay_amount` decimal(16,2) DEFAULT NULL,
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_order
-- ----------------------------

-- ----------------------------
-- Table structure for tb_pay
-- ----------------------------
DROP TABLE IF EXISTS `tb_pay`;
CREATE TABLE `tb_pay` (
  `id` bigint(20) NOT NULL,
  `saler_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `buyer_code` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `remark` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0,待支付,1,确认支付,2,待卖家确认,3,支付成功',
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pay
-- ----------------------------

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product` (
  `id` bigint(20) NOT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `market_price` decimal(16,2) DEFAULT NULL,
  `price` decimal(16,2) NOT NULL,
  `cate` bigint(20) DEFAULT NULL,
  `spec` varchar(32) DEFAULT NULL,
  `brand` varchar(32) DEFAULT NULL,
  `store_num` decimal(16,2) DEFAULT NULL,
  `pic_main` varchar(512) DEFAULT NULL,
  `pic1` varchar(512) DEFAULT NULL,
  `pic2` varchar(512) DEFAULT NULL,
  `pic3` varchar(512) DEFAULT NULL,
  `sort_no` int(11) DEFAULT NULL COMMENT '越大越靠前',
  `description` longtext,
  `status` int(11) DEFAULT NULL COMMENT '1,上架，0下架',
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_product
-- ----------------------------

-- ----------------------------
-- Table structure for tb_shop
-- ----------------------------
DROP TABLE IF EXISTS `tb_shop`;
CREATE TABLE `tb_shop` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `code` varchar(32) NOT NULL COMMENT '不能重复',
  `name` varchar(32) NOT NULL,
  `logo` varchar(512) DEFAULT NULL,
  `sort_no` int(11) DEFAULT NULL COMMENT '越大越靠前',
  `status` int(11) DEFAULT NULL COMMENT '1,正常，0冻结',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `zhifubao_qr_code` varchar(512) DEFAULT NULL,
  `weixin_qr_code` varchar(512) DEFAULT NULL,
  `zhifubao_qr_code2` varchar(512) DEFAULT NULL,
  `weixin_qr_code2` varchar(512) DEFAULT NULL,
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_shop
-- ----------------------------

-- ----------------------------
-- Table structure for tb_trip
-- ----------------------------
DROP TABLE IF EXISTS `tb_trip`;
CREATE TABLE `tb_trip` (
  `id` bigint(20) NOT NULL,
  `saler_id` bigint(20) DEFAULT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `shop_name` varchar(32) DEFAULT NULL,
  `target_address` varchar(32) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `remark` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0,未出发,1,已出发,2,已回',
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_trip
-- ----------------------------

-- ----------------------------
-- Table structure for tb_trip_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_trip_product`;
CREATE TABLE `tb_trip_product` (
  `id` bigint(20) NOT NULL,
  `shop_id` bigint(20) DEFAULT NULL,
  `trip_id` bigint(20) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `market_price` decimal(16,2) DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  `cate` bigint(20) DEFAULT NULL,
  `spec` varchar(32) DEFAULT NULL,
  `brand` varchar(32) DEFAULT NULL,
  `store_num` decimal(16,2) DEFAULT NULL,
  `pic_main` varchar(512) DEFAULT NULL,
  `pic1` varchar(512) DEFAULT NULL,
  `pic2` varchar(512) DEFAULT NULL,
  `pic3` varchar(512) DEFAULT NULL,
  `sort_no` int(11) DEFAULT NULL COMMENT '越大越靠前',
  `description` longtext,
  `status` int(11) DEFAULT NULL COMMENT '1,上架，0下架',
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_trip_product
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` bigint(20) NOT NULL,
  `login_name` varchar(32) NOT NULL COMMENT '目前与手机号一致',
  `name` varchar(32) DEFAULT NULL,
  `pwd` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0,超级管理员,1，卖家，2，买家',
  `status` int(11) DEFAULT NULL COMMENT '1,正常，0冻结',
  `email` varchar(32) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `weixin` varchar(32) DEFAULT NULL,
  `zhifubao` varchar(32) DEFAULT NULL,
  `qq` varchar(32) DEFAULT NULL,
  `weixin_qr_code` varchar(32) DEFAULT NULL,
  `zhifubao_qr_code` varchar(32) DEFAULT NULL,
  `qq_qr_code` varchar(32) DEFAULT NULL,
  `id_card` varchar(32) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL COMMENT '1，男，0，女',
  `address` varchar(512) DEFAULT NULL COMMENT '详细地址',
  `creator_id` varchar(32) DEFAULT NULL,
  `created_time` varchar(32) DEFAULT NULL,
  `modifier_id` varchar(32) DEFAULT NULL,
  `modified_time` varchar(32) DEFAULT NULL,
  `dr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_name` (`login_name`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
