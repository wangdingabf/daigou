<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.daigou.dao.ProductMapper">
  <resultMap id="BaseResultMap" type="com.daigou.model.Product">
    <id column="id" jdbcType="BIGINT" property="id" />
    <result column="saler_id" jdbcType="BIGINT" property="salerId" />
    <result column="shop_id" jdbcType="BIGINT" property="shopId" />
    <result column="name" jdbcType="VARCHAR" property="name" />
    <result column="market_price" jdbcType="DECIMAL" property="marketPrice" />
    <result column="cost_price" jdbcType="DECIMAL" property="costPrice" />
    <result column="sale_price" jdbcType="DECIMAL" property="salePrice" />
    <result column="cate" jdbcType="VARCHAR" property="cate" />
    <result column="spec" jdbcType="VARCHAR" property="spec" />
    <result column="brand" jdbcType="VARCHAR" property="brand" />
    <result column="production_place" jdbcType="VARCHAR" property="productionPlace" />
    <result column="store_num" jdbcType="DECIMAL" property="storeNum" />
    <result column="pic_main" jdbcType="VARCHAR" property="picMain" />
    <result column="pic1" jdbcType="VARCHAR" property="pic1" />
    <result column="pic2" jdbcType="VARCHAR" property="pic2" />
    <result column="pic3" jdbcType="VARCHAR" property="pic3" />
    <result column="sort_no" jdbcType="INTEGER" property="sortNo" />
    <result column="status" jdbcType="INTEGER" property="status" />
    <result column="creator_id" jdbcType="BIGINT" property="creatorId" />
    <result column="created_time" jdbcType="TIMESTAMP" property="createdTime" />
    <result column="modifier_id" jdbcType="BIGINT" property="modifierId" />
    <result column="modified_time" jdbcType="TIMESTAMP" property="modifiedTime" />
    <result column="dr" jdbcType="INTEGER" property="dr" />
  </resultMap>
  <resultMap extends="BaseResultMap" id="ResultMapWithBLOBs" type="com.daigou.model.Product">
    <result column="description" jdbcType="LONGVARCHAR" property="description" />
  </resultMap>
  <sql id="Base_Column_List">
    id, saler_id, shop_id, name, market_price, cost_price, sale_price, cate, spec, brand, 
    production_place, store_num, pic_main, pic1, pic2, pic3, sort_no, status, creator_id, 
    created_time, modifier_id, modified_time, dr
  </sql>
  <sql id="Simple_Column_List">
    id, saler_id, shop_id, name, market_price, sale_price, cate, spec, brand, 
    production_place, pic_main, status
  </sql>
  <sql id="Blob_Column_List">
    description
  </sql>
  <select id="selectByPrimaryKey" parameterType="java.lang.Long" resultMap="ResultMapWithBLOBs">
    select 
    <include refid="Base_Column_List" />
    ,
    <include refid="Blob_Column_List" />
    from tb_product
    where id = #{id,jdbcType=BIGINT}
  </select>
  <select id="selectByIds" parameterType="java.lang.String" resultMap="ResultMapWithBLOBs">
    select 
    <include refid="Base_Column_List" />
    from tb_product
    where id in (#{ids,jdbcType=VARCHAR})
  </select>
  <select id="selectCaogaoByShopId" resultMap="BaseResultMap" parameterType="java.util.Map">
    select 
    <include refid="Simple_Column_List" />
    from tb_product
    where shop_id = #{shopId,jdbcType=BIGINT}
    and dr = 0
    <if test="searchKey != null and searchKey !=''">
    and (
	   	name like concat(#{searchKey,jdbcType=VARCHAR},'%')
	   	or
	    spec like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    cate like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    brand like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    production_place like concat(#{searchKey,jdbcType=VARCHAR},'%')
    )
    </if>
    order by created_time desc
  </select>
  <select id="selectByShopId" resultMap="BaseResultMap" parameterType="java.util.Map">
    select 
    <include refid="Simple_Column_List" />
    from tb_product
    where shop_id = #{shopId,jdbcType=BIGINT}
    and dr = 1
    <if test="status != null">
    and status = #{status,jdbcType=INTEGER}
    </if>
    <if test="searchKey != null and searchKey !=''">
    and (
	   	name like concat(#{searchKey,jdbcType=VARCHAR},'%')
	   	or
	    spec like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    cate like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    brand like concat(#{searchKey,jdbcType=VARCHAR},'%')
	    or
	    production_place like concat(#{searchKey,jdbcType=VARCHAR},'%')
    )
    </if>
    order by status desc,sort_no desc,created_time desc
  </select>
  <select id="selectTotalCountByShopId" parameterType="java.lang.Long" resultType="java.lang.Integer">
    select count(1) from tb_product where shop_id = #{shopId,jdbcType=BIGINT} and status in(0,1) and dr = 1
  </select>
  <select id="selectOnSaleCountByShopId" parameterType="java.lang.Long" resultType="java.lang.Integer">
    select count(1) from tb_product where shop_id = #{shopId,jdbcType=BIGINT} and status = 1 and dr = 1
  </select>
  <select id="selectOffSaleCountByShopId" parameterType="java.lang.Long" resultType="java.lang.Integer">
    select count(1) from tb_product where shop_id = #{shopId,jdbcType=BIGINT} and status = 0 and dr = 1
  </select>
  <select id="selectCaogaoCountByShopId" parameterType="java.lang.Long" resultType="java.lang.Integer">
    select count(1) from tb_product where shop_id = #{shopId,jdbcType=BIGINT} and dr = 0
  </select>
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
    delete from tb_product
    where id = #{id,jdbcType=BIGINT}
  </delete>
  <insert id="insert" parameterType="com.daigou.model.Product">
    insert into tb_product (id, saler_id, shop_id, 
      name, market_price, cost_price, 
      sale_price, cate, spec, 
      brand, production_place, store_num, 
      pic_main, pic1, pic2, 
      pic3, sort_no, status, 
      creator_id, created_time, modifier_id, 
      modified_time, dr, description
      )
    values (#{id,jdbcType=BIGINT}, #{salerId,jdbcType=BIGINT}, #{shopId,jdbcType=BIGINT}, 
      #{name,jdbcType=VARCHAR}, #{marketPrice,jdbcType=DECIMAL}, #{costPrice,jdbcType=DECIMAL}, 
      #{salePrice,jdbcType=DECIMAL}, #{cate,jdbcType=VARCHAR}, #{spec,jdbcType=VARCHAR}, 
      #{brand,jdbcType=VARCHAR}, #{productionPlace,jdbcType=VARCHAR}, #{storeNum,jdbcType=DECIMAL}, 
      #{picMain,jdbcType=VARCHAR}, #{pic1,jdbcType=VARCHAR}, #{pic2,jdbcType=VARCHAR}, 
      #{pic3,jdbcType=VARCHAR}, #{sortNo,jdbcType=INTEGER}, #{status,jdbcType=INTEGER}, 
      #{creatorId,jdbcType=BIGINT}, #{createdTime,jdbcType=TIMESTAMP}, #{modifierId,jdbcType=BIGINT}, 
      #{modifiedTime,jdbcType=TIMESTAMP}, #{dr,jdbcType=INTEGER}, #{description,jdbcType=LONGVARCHAR}
      )
  </insert>
  <insert id="insertSelective" parameterType="com.daigou.model.Product">
    insert into tb_product
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <if test="id != null">
        id,
      </if>
      <if test="salerId != null">
        saler_id,
      </if>
      <if test="shopId != null">
        shop_id,
      </if>
      <if test="name != null">
        name,
      </if>
      <if test="marketPrice != null">
        market_price,
      </if>
      <if test="costPrice != null">
        cost_price,
      </if>
      <if test="salePrice != null">
        sale_price,
      </if>
      <if test="cate != null">
        cate,
      </if>
      <if test="spec != null">
        spec,
      </if>
      <if test="brand != null">
        brand,
      </if>
      <if test="productionPlace != null">
        production_place,
      </if>
      <if test="storeNum != null">
        store_num,
      </if>
      <if test="picMain != null">
        pic_main,
      </if>
      <if test="pic1 != null">
        pic1,
      </if>
      <if test="pic2 != null">
        pic2,
      </if>
      <if test="pic3 != null">
        pic3,
      </if>
      <if test="sortNo != null">
        sort_no,
      </if>
      <if test="status != null">
        status,
      </if>
      <if test="creatorId != null">
        creator_id,
      </if>
      <if test="createdTime != null">
        created_time,
      </if>
      <if test="modifierId != null">
        modifier_id,
      </if>
      <if test="modifiedTime != null">
        modified_time,
      </if>
      <if test="dr != null">
        dr,
      </if>
      <if test="description != null">
        description,
      </if>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
      <if test="id != null">
        #{id,jdbcType=BIGINT},
      </if>
      <if test="salerId != null">
        #{salerId,jdbcType=BIGINT},
      </if>
      <if test="shopId != null">
        #{shopId,jdbcType=BIGINT},
      </if>
      <if test="name != null">
        #{name,jdbcType=VARCHAR},
      </if>
      <if test="marketPrice != null">
        #{marketPrice,jdbcType=DECIMAL},
      </if>
      <if test="costPrice != null">
        #{costPrice,jdbcType=DECIMAL},
      </if>
      <if test="salePrice != null">
        #{salePrice,jdbcType=DECIMAL},
      </if>
      <if test="cate != null">
        #{cate,jdbcType=VARCHAR},
      </if>
      <if test="spec != null">
        #{spec,jdbcType=VARCHAR},
      </if>
      <if test="brand != null">
        #{brand,jdbcType=VARCHAR},
      </if>
      <if test="productionPlace != null">
        #{productionPlace,jdbcType=VARCHAR},
      </if>
      <if test="storeNum != null">
        #{storeNum,jdbcType=DECIMAL},
      </if>
      <if test="picMain != null">
        #{picMain,jdbcType=VARCHAR},
      </if>
      <if test="pic1 != null">
        #{pic1,jdbcType=VARCHAR},
      </if>
      <if test="pic2 != null">
        #{pic2,jdbcType=VARCHAR},
      </if>
      <if test="pic3 != null">
        #{pic3,jdbcType=VARCHAR},
      </if>
      <if test="sortNo != null">
        #{sortNo,jdbcType=INTEGER},
      </if>
      <if test="status != null">
        #{status,jdbcType=INTEGER},
      </if>
      <if test="creatorId != null">
        #{creatorId,jdbcType=BIGINT},
      </if>
      <if test="createdTime != null">
        #{createdTime,jdbcType=TIMESTAMP},
      </if>
      <if test="modifierId != null">
        #{modifierId,jdbcType=BIGINT},
      </if>
      <if test="modifiedTime != null">
        #{modifiedTime,jdbcType=TIMESTAMP},
      </if>
      <if test="dr != null">
        #{dr,jdbcType=INTEGER},
      </if>
      <if test="description != null">
        #{description,jdbcType=LONGVARCHAR},
      </if>
    </trim>
  </insert>
  <update id="updateByPrimaryKeySelective" parameterType="com.daigou.model.Product">
    update tb_product
    <set>
      <if test="salerId != null">
        saler_id = #{salerId,jdbcType=BIGINT},
      </if>
      <if test="shopId != null">
        shop_id = #{shopId,jdbcType=BIGINT},
      </if>
      <if test="name != null">
        name = #{name,jdbcType=VARCHAR},
      </if>
      <if test="marketPrice != null">
        market_price = #{marketPrice,jdbcType=DECIMAL},
      </if>
      <if test="costPrice != null">
        cost_price = #{costPrice,jdbcType=DECIMAL},
      </if>
      <if test="salePrice != null">
        sale_price = #{salePrice,jdbcType=DECIMAL},
      </if>
      <if test="cate != null">
        cate = #{cate,jdbcType=VARCHAR},
      </if>
      <if test="spec != null">
        spec = #{spec,jdbcType=VARCHAR},
      </if>
      <if test="brand != null">
        brand = #{brand,jdbcType=VARCHAR},
      </if>
      <if test="productionPlace != null">
        production_place = #{productionPlace,jdbcType=VARCHAR},
      </if>
      <if test="storeNum != null">
        store_num = #{storeNum,jdbcType=DECIMAL},
      </if>
      <if test="picMain != null">
        pic_main = #{picMain,jdbcType=VARCHAR},
      </if>
      <if test="pic1 != null">
        pic1 = #{pic1,jdbcType=VARCHAR},
      </if>
      <if test="pic2 != null">
        pic2 = #{pic2,jdbcType=VARCHAR},
      </if>
      <if test="pic3 != null">
        pic3 = #{pic3,jdbcType=VARCHAR},
      </if>
      <if test="sortNo != null">
        sort_no = #{sortNo,jdbcType=INTEGER},
      </if>
      <if test="status != null">
        status = #{status,jdbcType=INTEGER},
      </if>
      <if test="creatorId != null">
        creator_id = #{creatorId,jdbcType=BIGINT},
      </if>
      <if test="createdTime != null">
        created_time = #{createdTime,jdbcType=TIMESTAMP},
      </if>
      <if test="modifierId != null">
        modifier_id = #{modifierId,jdbcType=BIGINT},
      </if>
      <if test="modifiedTime != null">
        modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
      </if>
      <if test="dr != null">
        dr = #{dr,jdbcType=INTEGER},
      </if>
      <if test="description != null">
        description = #{description,jdbcType=LONGVARCHAR},
      </if>
    </set>
    where id = #{id,jdbcType=BIGINT}
  </update>
  <update id="updateByPrimaryKeyWithBLOBs" parameterType="com.daigou.model.Product">
    update tb_product
    set saler_id = #{salerId,jdbcType=BIGINT},
      shop_id = #{shopId,jdbcType=BIGINT},
      name = #{name,jdbcType=VARCHAR},
      market_price = #{marketPrice,jdbcType=DECIMAL},
      cost_price = #{costPrice,jdbcType=DECIMAL},
      sale_price = #{salePrice,jdbcType=DECIMAL},
      cate = #{cate,jdbcType=VARCHAR},
      spec = #{spec,jdbcType=VARCHAR},
      brand = #{brand,jdbcType=VARCHAR},
      production_place = #{productionPlace,jdbcType=VARCHAR},
      store_num = #{storeNum,jdbcType=DECIMAL},
      pic_main = #{picMain,jdbcType=VARCHAR},
      pic1 = #{pic1,jdbcType=VARCHAR},
      pic2 = #{pic2,jdbcType=VARCHAR},
      pic3 = #{pic3,jdbcType=VARCHAR},
      sort_no = #{sortNo,jdbcType=INTEGER},
      status = #{status,jdbcType=INTEGER},
      creator_id = #{creatorId,jdbcType=BIGINT},
      created_time = #{createdTime,jdbcType=TIMESTAMP},
      modifier_id = #{modifierId,jdbcType=BIGINT},
      modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
      dr = #{dr,jdbcType=INTEGER},
      description = #{description,jdbcType=LONGVARCHAR}
    where id = #{id,jdbcType=BIGINT}
  </update>
  <update id="updateByPrimaryKey" parameterType="com.daigou.model.Product">
    update tb_product
    set saler_id = #{salerId,jdbcType=BIGINT},
      shop_id = #{shopId,jdbcType=BIGINT},
      name = #{name,jdbcType=VARCHAR},
      market_price = #{marketPrice,jdbcType=DECIMAL},
      cost_price = #{costPrice,jdbcType=DECIMAL},
      sale_price = #{salePrice,jdbcType=DECIMAL},
      cate = #{cate,jdbcType=VARCHAR},
      spec = #{spec,jdbcType=VARCHAR},
      brand = #{brand,jdbcType=VARCHAR},
      production_place = #{productionPlace,jdbcType=VARCHAR},
      store_num = #{storeNum,jdbcType=DECIMAL},
      pic_main = #{picMain,jdbcType=VARCHAR},
      pic1 = #{pic1,jdbcType=VARCHAR},
      pic2 = #{pic2,jdbcType=VARCHAR},
      pic3 = #{pic3,jdbcType=VARCHAR},
      sort_no = #{sortNo,jdbcType=INTEGER},
      status = #{status,jdbcType=INTEGER},
      creator_id = #{creatorId,jdbcType=BIGINT},
      created_time = #{createdTime,jdbcType=TIMESTAMP},
      modifier_id = #{modifierId,jdbcType=BIGINT},
      modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
      dr = #{dr,jdbcType=INTEGER}
    where id = #{id,jdbcType=BIGINT}
  </update>
</mapper>