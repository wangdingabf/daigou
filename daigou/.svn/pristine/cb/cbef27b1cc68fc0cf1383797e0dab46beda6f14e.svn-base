package com.daigou.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.ShopMapper;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.ShopService;
import com.daigou.service.UserService;
import com.daigou.tools.CacheManagerTool;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: ShopServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("shopService")  
public class ShopServiceImpl implements ShopService {
	@Autowired
	private CacheManagerTool cacheManagerTool;
	@Autowired  
	protected ShopMapper shopMapper;
	
	@Autowired
	private UserService userService;
	
	public int deleteByPrimaryKey(Long id){
		return shopMapper.deleteByPrimaryKey(id);
	}

	public ServiceResult insert(User user,Shop record){
		//转换成小写存储-手机端比较难操作，所以需要去空格
		record.setCode(record.getCode().trim().toLowerCase());
		Shop shopDB = shopMapper.selectByCode(record.getCode());
		if(shopDB != null){
			return new ServiceResult(false,"亲,店铺编码已存在,请换一个更新颖的吧!");
		}
		record.setId(GenerateUUID.generateUUID());
		if(record.getDescription() == null || "".equals(record.getDescription())){
			record.setDescription("店主很懒，什么也没留下......");
		}
		userService.updateByPrimaryKeySelective(user);
		record.setStatus(1);
		Date now = new Date();
		record.setCreatorId(user.getId());
		record.setCreatedTime(now);
		record.setStartTime(now);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.add(Calendar.MONTH, 3);
		record.setEndTime(calendar.getTime());
		shopMapper.insert(record);
		return new ServiceResult(true,"店铺完善成功!");
	}

	public int insertSelective(Shop record){
		return shopMapper.insertSelective(record);
	}

	public Shop selectByPrimaryKey(Long id){
		Shop shop = cacheManagerTool.getShopByKey(CacheManagerTool.SHOP_CACHE_BY_ID,id.toString());
		if(shop == null){
			shop = shopMapper.selectByPrimaryKey(id);
			cacheManagerTool.putShopCache(CacheManagerTool.SHOP_CACHE_BY_ID,id.toString(), shop);
		}
		return shop;
	}
	
	public Shop selectByCode(String code){
		Shop shop = cacheManagerTool.getShopByKey(CacheManagerTool.SHOP_CACHE_BY_CODE,code);
		if(shop == null){
			shop = shopMapper.selectByCode(code);
			cacheManagerTool.putShopCache(CacheManagerTool.SHOP_CACHE_BY_CODE,code, shop);
		}
		return shop;
		
	}
    
	public Shop selectBySalerId(Long salerId){
		return shopMapper.selectBySalerId(salerId);
	}

	public int updateByPrimaryKeySelective(Shop record){
		record.setCode(null);
		shopMapper.updateByPrimaryKeySelective(record);
		record = shopMapper.selectByPrimaryKey(record.getId());
		cacheManagerTool.putShopCache(CacheManagerTool.SHOP_CACHE_BY_ID,record.getId().toString(), record);
		cacheManagerTool.putShopCache(CacheManagerTool.SHOP_CACHE_BY_CODE,record.getCode(), record);
		return 1;
	}

	public int updateByPrimaryKey(Shop record){
		return shopMapper.updateByPrimaryKey(record);
	}
	/**
	 * 前端用的，返回了商品数量
	 */
	public List<Shop> selectAllShop(){
		return shopMapper.selectAllShop();
	}
}  