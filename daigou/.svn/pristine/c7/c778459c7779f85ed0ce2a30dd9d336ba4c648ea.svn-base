package com.daigou.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.dto.AjaxResult;
import com.daigou.model.User;
import com.daigou.service.UserService;

/**
 * 
 * 类名称：LoginController   
 * 创建人：李志   
 * 创建时间：2018年02月14日  上午12:16:42  
 * 修改人：李志   
 * 修改时间：2018年04月14日 00:35:17   
 * @version    
 *
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController{
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/index",method = RequestMethod.GET)
    public String toIndexPage(){
		LOGGER.info("进入后台管理首页");
        return "/admin/app/index";
    }
	
	
	@RequestMapping(value = "/done_user/{id}",method = RequestMethod.GET)
    public String toDoneUserPage(@PathVariable("id") Long id){
		LOGGER.info("进入完善用户信息页面");
		User user = userService.selectByPrimaryKey(id);
		//状态,0,冻结，10，待完善user，11，待完善shop,20,ok
		if(user.getStatus() == 0){
			return "/admin/app/login";
		}else if(user.getStatus() == 10){
			return "/admin/app/done_user";
		}else if(user.getStatus() == 11){
			return "/admin/app/done_shop";
		}else{
			return toIndexPage();
		}
    }
	
	@RequestMapping(value = "/done_shop/{id}",method = RequestMethod.GET)
    public String toDoneShopPage(@PathVariable("id") Long id){
		LOGGER.info("进入完善商铺信息页面");
		User user = userService.selectByPrimaryKey(id);
		//状态,0,冻结，10，待完善user，11，待完善shop,20,ok
		if(user.getStatus() == 0){
			return "/admin/app/login";
		}else if(user.getStatus() == 10){
			return "/admin/app/done_user";
		}else if(user.getStatus() == 11){
			return "/admin/app/done_shop";
		}else{
			return toIndexPage();
		}
    }
	@RequestMapping(value = "/done_user",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updateDoneUser(@RequestBody User user){
		LOGGER.info("进入完善用户信息页面");
		userService.updateByPrimaryKeySelective(user);
        return new AjaxResult(true,"");
    }
	
	@RequestMapping(value = "/done_shop",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult updateDoneShop(@RequestBody User user){
		LOGGER.info("进入完善商铺信息页面");
		return new AjaxResult(true,"");
    }
}
